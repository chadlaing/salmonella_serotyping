#!/usr/bin/perl

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../../";
use Modules::STyper;

my $app = Modules::STyper->new(PARAM=>'client');
$app->run();