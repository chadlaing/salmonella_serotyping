#!/usr/bin/env perl

package Roles::Settings;

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../";
use Role::Tiny;

sub outputDirectory{
	my $self=shift;
	$self->{'_outputDirectory'}=shift // return $self->{'_outputDirectory'};
}

sub dbDbi{
	my $self=shift;
	$self->{'_dbDbi'}=shift // return $self->{'_dbDbi'};
}

sub dbName{
	my $self=shift;
	$self->{'_dbName'}=shift // return $self->{'_dbName'};
}

sub dbHost{
	my $self=shift;
	$self->{'_dbHost'}=shift // return $self->{'_dbHost'};
}

sub dbPort{
	my $self=shift;
	$self->{'_dbPort'}=shift // return $self->{'_dbPort'};
}

sub dbUser{
	my $self=shift;
	$self->{'_dbUser'}=shift // return $self->{'_dbUser'};
}

sub dbPassword{
	my $self=shift;
	$self->{'_dbPassword'}=shift // return $self->{'_dbPassword'};
}

sub newickCore{
	my $self=shift;
	$self->{'_newickCore'}=shift // return $self->{'_newickCore'};
}

sub mistDirectory{
	my $self=shift;
	$self->{'_mistDirectory'}=shift // return $self->{'_mistDirectory'};
}

sub assaysDirectory{
	my $self=shift;
	$self->{'_assaysDirectory'}=shift // return $self->{'_assaysDirectory'};
}

sub loadConfigFile{
	my $self=shift;

	unless(scalar(@_)==1){
		die "Wrong number of arguments sent to loadConfigFile\n";
	}
	my $file = shift;

	my $inFH = IO::File->new('<' . $file) or die "Cannot open $file";
	 while(my $line = $inFH->getline){
	 	#remove all newline chars
	 	$line =~ s/\R//g;

	 	my @la = split('\t',$line);
	 	my $setting = $la[0];
	 	my $value = $la[1];

	 	#cannot use an array index for an assignment to a class method
	 	#that is why we have $setting and $value created

	 	if($self->can($setting)){
	 		$self->$setting($value);
	 	}
	 	else{
	 		warn("$la[0] is not a valid configuration setting\n");
	 	}	
	 }
	$inFH->close();
}

1;