#!/usr/bin/env perl

package Roles::Database;

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../";
use STyper::Schema;
use Role::Tiny;
use IO::File;

sub connectDatabase{
	my $self=shift;

	my $file=shift;

	#use Roles::Settings for the info
	#connect dbixSchema
	#'dbi:Pg:dbname=styper;host=localhost;port=5433','name','password'
	my $dataSource = 
		'dbi:' . $self->dbDbi .
		':database=' . $self->dbName . 
		';host=' . $self->dbHost . 
		';port=' . $self->dbPort;

	my $dbHandle = STyper::Schema->connect($dataSource, $self->dbUser,$self->dbPassword) or die "Could not connect to database";		
	$self->dbixSchema($dbHandle);
}

sub dbixSchema{
	my $self=shift;
	$self->{'_dbixSchema'}=shift // return $self->{'_dbixSchema'};
}


1;