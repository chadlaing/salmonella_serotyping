#!/usr/bin/env perl

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../";
use RDF::Sesame;

my $sesame = RDF::Sesame->connect(
	port=>8080,
	host=>'http://localhost',
	directory=>'openrdf-sesame'
) or die "Couldn't connect $!";

my @repos = $sesame->repositories;

foreach my $repo(@repos){
	print $repo . "\n";
}

print "Danished\n";