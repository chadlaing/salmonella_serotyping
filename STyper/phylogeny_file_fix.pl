#!/usr/bin/perl

#take a phylip alignment and output the fasta equivalent

use FindBin;
use lib "$FindBin::Bin/../../";
use warnings;
use strict;
use diagnostics;
use IO::File;

my $file = $ARGV[0];

my $inFH = IO::File->new('<'. $file) or die "$!";

my $counter=0;
while(my $line=$inFH->getline){
	if($counter==0){
		$counter++;
		next;
		#skip the phylip header
	}

	if($line =~ m/^(\d+)/){
		my $id = $1;
		$line =~ s/^\d+\s+/>$id\n/;
	}
	print $line;
}
$inFH->close();