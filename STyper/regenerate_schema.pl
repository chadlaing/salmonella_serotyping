#!/usr/bin/env perl

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../";
use IO::File;
use JSON;
use File::Slurp;


my @schema=(
	'AMR',
	'Antigen',
	'CRISPOL',
	'H0',
	'H1',
	'H2',
	'MLST',
	'MLVA',
	'Misc',
	'Serogroup',
	'Serotyping',
	'Serovar'
);

my $markersDirectory = $ARGV[0];
my $mistDirectory = $ARGV[1];

print "Marker directory: $markersDirectory\nmist directory: $mistDirectory\n";

#get all markers files from directory
opendir( DIRECTORY, $markersDirectory ) or die "cannot open directory $markersDirectory $!\n";
my @dir = readdir DIRECTORY;
closedir DIRECTORY;

my @fileNames;
foreach my $fileName(@dir){
    unless($fileName =~ m/\.markers/){
    	next;
    }
    push @fileNames, ( $markersDirectory . $fileName );
}



#all the metadata for all the assays is now auto generated, so any strain will work
my $testFileName = '/home/chad/salmonella_genomes/CAGR.fasta';

#create test file for running mist
#currently MIST requires a hit to generate any output
#keep this if that behaviour changes
#my $testFileName = $mistDirectory . 'testfile.fasta';
# my $testFH = IO::File->new('>' . $testFileName) or die "$!";
# $testFH->print('>test' . "\n" . 'CCACCGGCATCCCGCGTCCGTCGTCAATCACTTCCAGCGATTGATCGGCATGTAAAATGACATCCACGCGTTTGGCGTGA
# CCTGCCAGTGCTTCATCCACACTATTATCAATTACTTCCTGACCCAAATGGTTGGGGCGGGTCGTATCGGTGTACATCCC
# CGGGCGGCGGCGTACCGGCTCAAGCCCAGTGAGTACCTCAATGGCATCAGCGTTATAAGTTTGCGTCATGGTTTAATTAG
# CAATTCGAATTGATTGTCAGCAACTGTGCAGTCCAAGAAAATCGACAATCTGGTTGAAATAATCTTCGAAGCCCGTGAAT
# GCGTGATTACCCCCCTCGGTCACTGTCTGACGGCAGGAGGCGTAATATGCCACCGCCTGGCGGTAATCCAGCACTTCATC
# GCCCGTCTGTTGCAGTAGCCAGATCAGGTCCGGCGCTTCCAGCGGGTCAATCTGCATGACTTTAAGATCATAAATATGGC
# GAGACTCTAGCACATATTGCTGCCCGGTGTAGGGGTTCTCGTTCTGACCGAGATAGTCGGTCAGTAATTCAAAGGGCCGC' . "\n");
#$testFH->close();

foreach my $file(@fileNames){
	print "File: $file\n";
	my $mistOutputJSON = _runMist($file);
	#my $mistOutputJSON = _tempJSONprovider();
	my $markersRef = _getMarkers($mistOutputJSON);
	my ($assay, $data) = _createSchema($markersRef,$file);

	my $outFH = IO::File->new('>' . $markersDirectory . $assay . '.pm') or die "Cannot create file $!";
	$outFH->print($data);
	$outFH->close();
}

# unlink $testFileName;




sub _tempJSONprovider{
	my $providedJsonText=q|
	{
  "$types": {
    "MIST.TypingResultsCollection, MIST, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null": "1",
    "MIST.TypingResults, MIST, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null": "2",
    "MIST.MarkerMatch, MIST, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null": "3",
    "MIST.BlastOutput, MIST, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null": "4",
    "MIST.Marker, MIST, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null": "5",
    "MIST.ExtraTestInfo, MIST, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null": "6"
  },
  "$type": "1",
  "Results": [
    {
      "$type": "2",
      "TestResults": {
        "MLST": {
          "PURE": {
            "$type": "3",
            "BlastResults": {
              "$type": "4",
              "QueryAln": "AGCGACTGGGCTACCATGCAATTCGCCGCCGAAATTTTTGAAATTCTGGATGTCCCGCACCATGTAGAAGTGGTTTCCGCCCATCGCACCCCCGATAAACTGTTCAGCTTCGCCGAAACGGCGGAAGAGAACGGATATCAAGTGATTATTGCCGGCGCGGGCGGCGCGGCACACCTGCCGGGAATGATTGCGGCAAAAACGCTGGTCCCGGTACTCGGCGTGCCGGTACAAAGCGCTGCGCTCAGCGGCGTGGATAGCCTCTACTCCATCGTGCAGATGCCGCGCGGCATTCCGGTGGGTACGCTGGCGATCGGTAAAGCCGGGGCGGCGAACGCCGCACTGCTGGCAGCGCAAATTTTGGCTACGCATGATAGCGCG---CTGCATCGGCGCATCGCCGAC",
              "SubjAln": "AGCGACTGGGCTACCATGCAATTCGCCGCCGAAATTTTTGAAATTCTGGATGTCCCGCACCATGTAGAAGTGGTTTCCGCTCATCGCACCCCCGATAAACTGTTCAGCTTCGCCGAAACGGCGGAAGAGAACGGATATCAAGTGATTATTGCCGGCGCGGGCGGCGCGGCGCACCTGCCGGGAATGATTGCGGCAAAAACGCTGGTCCCGGTACTCGGCGTGCCGGTACAAAGCGCTGCGCTAAGCGGCGTGGATAGCCTCTACTCCATCGTGCAGATGCCGCGCGGCATTCCGGTGGGTACGCTGGCGATCGGTAAAGCCGGTGCCGCTAACGCCGCCCTGCTCGCCGCGCAGATTCTGG---CGCAACACGACGCGGAACTGCATCAGCGCATTGCCGAC",
              "Gaps": 6,
              "QueryName": "0",
              "SubjectName": "0",
              "PercentIdentity": 94.03,
              "SubjectStartIndex": 597175,
              "SubjectEndIndex": 597573,
              "ReverseComplement": true,
              "AlignmentLength": 402,
              "BitScore": 614,
              "QueryLength": 399,
              "QueryEndIndex": 399,
              "QueryStartIndex": 1
            },
            "ForwardPrimerBlastResult": null,
            "ReversePrimerBlastResult": null,
            "StrainName": "Se_Typhimurium_LT2",
            "MarkerName": "PURE",
            "TestName": "MLST",
            "MarkerCall": "5",
            "AlleleMatch": "PURE5",
            "ContigMatchName": "NC_003197_Salmonella_enterica_subsp._enterica_serovar_Typhimurium_str._LT2_chromosome,_complete_genome.",
            "Mismatches": 0,
            "BlastPercentIdentity": 94.03,
            "BlastAlignmentLength": 402,
            "BlastGaps": 6,
            "AmpliconSize": 399,
            "CorrectMarkerMatch": true,
            "StartIndex": 597175,
            "EndIndex": 597573,
            "Amplicon": "AGCGACTGGGCTACCATGCAATTCGCCGCCGAAATTTTTGAAATTCTGGATGTCCCGCACCATGTAGAAGTGGTTTCCGCTCATCGCACCCCCGATAAACTGTTCAGCTTCGCCGAAACGGCGGAAGAGAACGGATATCAAGTGATTATTGCCGGCGCGGGCGGCGCGGCGCACCTGCCGGGAATGATTGCGGCAAAAACGCTGGTCCCGGTACTCGGCGTGCCGGTACAAAGCGCTGCGCTAAGCGGCGTGGATAGCCTCTACTCCATCGTGCAGATGCCGCGCGGCATTCCGGTGGGTACGCTGGCGATCGGTAAAGCCGGTGCCGCTAACGCCGCCCTGCTCGCCGCGCAGATTCTGGCGCAACACGACGCGGAACTGCATCAGCGCATTGCCGAC",
            "Marker": {
              "$type": "5",
              "Selected": true,
              "TestName": "MLST",
              "TypingTest": "Allelic",
              "Name": "PURE",
              "ForwardPrimer": "",
              "ReversePrimer": "",
              "AmpliconSize": -1,
              "AmpliconRange": 0,
              "AllelicDatabaseFilename": "/home/peter/mist_cmd_wiki/MIST_Senterica_MLST_test/alleles/PURE.fas",
              "RepeatSize": 0
            }
          },
          "SUCA": {
            "$type": "3",
            "BlastResults": {
              "$type": "4",
              "QueryAln": "AAACGCTTCCTGAACGAACTGACCGCCGCTGAAGGGCTGGAACGTTATCTGGGCGCCAAATTCCCGGGTGCGAAACGTTTCTCGCTCGAGGGGGGAGATGCGCTGATACCTATGCTGAAAGAGATGGTTCGCCATGCGGGTAACAGCGGCACTCGCGAAGTGGTGCTGGGGATGGCGCACCGCGGTCGTCTGAACGTGCTGATCAACGTACTGGGTAAAAAACCGCAGGATCTGTTCGACGAGTTTGCCGGTAAACATAAAGAACATCTGGGTACCGGCGACGTGAAGTATCACATGGGCTTCTCGTCAGATATCGAAACTGAAGGCGGTCTGGTTCACCTGGCGCTGGCGTTTAACCCATCGCATCTGGAAATTGTGAGCCCGGTGGTGATGGGCTCCGTGCGCGCCCGTCTGGACCGACTGGACGAACCGAGCAGTAATAAAGTGCTGCCGATCACTATTCACGGCGACGCCGCGGTGACCGGCCAGGGCGTGGTTCAG",
              "SubjAln": "AAACGCTTCCTGAACGAACTGACCGCCGCTGAAGGGCTGGAACGTTATCTGGGCGCCAAATTCCCGGGTGCGAAACGTTTCTCGCTCGAGGGGGGAGATGCGCTGATACCCATGCTGAAAGAGATGGTTCGCCATGCGGGTAACAGCGGCACTCGCGAAGTGGTGCTGGGGATGGCGCACCGCGGTCGCCTGAACGTGCTGATCAACGTACTGGGTAAAAAACCGCAGGATCTGTTCGACGAATTTGCCGGTAAGCATAAAGAACATCTGGGTACCGGCGACGTGAAGTATCACATGGGCTTCTCGTCAGATATCGAAACCGAAGGCGGTCTGGTTCACCTGGCGCTGGCGTTTAACCCATCGCATCTGGAAATTGTGAGCCCGGTGGTGATGGGCTCCGTGCGCGCCCGTCTGGACAGACTGGACGAACCGAGCAGCAACAAAGTGTTGCCGATCACTATTCACGGCGACGCCGCGGTGACCGGCCAGGGCGTGGTTCAG",
              "Gaps": 0,
              "QueryName": "0",
              "SubjectName": "0",
              "PercentIdentity": 98.2,
              "SubjectStartIndex": 802344,
              "SubjectEndIndex": 802844,
              "ReverseComplement": false,
              "AlignmentLength": 501,
              "BitScore": 863,
              "QueryLength": 501,
              "QueryEndIndex": 501,
              "QueryStartIndex": 1
            },
            "ForwardPrimerBlastResult": null,
            "ReversePrimerBlastResult": null,
            "StrainName": "Se_Typhimurium_LT2",
            "MarkerName": "SUCA",
            "TestName": "MLST",
            "MarkerCall": "9",
            "AlleleMatch": "SUCA9",
            "ContigMatchName": "NC_003197_Salmonella_enterica_subsp._enterica_serovar_Typhimurium_str._LT2_chromosome,_complete_genome.",
            "Mismatches": 0,
            "BlastPercentIdentity": 98.2,
            "BlastAlignmentLength": 501,
            "BlastGaps": 0,
            "AmpliconSize": 501,
            "CorrectMarkerMatch": true,
            "StartIndex": 802344,
            "EndIndex": 802844,
            "Amplicon": "AAACGCTTCCTGAACGAACTGACCGCCGCTGAAGGGCTGGAACGTTATCTGGGCGCCAAATTCCCGGGTGCGAAACGTTTCTCGCTCGAGGGGGGAGATGCGCTGATACCCATGCTGAAAGAGATGGTTCGCCATGCGGGTAACAGCGGCACTCGCGAAGTGGTGCTGGGGATGGCGCACCGCGGTCGCCTGAACGTGCTGATCAACGTACTGGGTAAAAAACCGCAGGATCTGTTCGACGAATTTGCCGGTAAGCATAAAGAACATCTGGGTACCGGCGACGTGAAGTATCACATGGGCTTCTCGTCAGATATCGAAACCGAAGGCGGTCTGGTTCACCTGGCGCTGGCGTTTAACCCATCGCATCTGGAAATTGTGAGCCCGGTGGTGATGGGCTCCGTGCGCGCCCGTCTGGACAGACTGGACGAACCGAGCAGCAACAAAGTGTTGCCGATCACTATTCACGGCGACGCCGCGGTGACCGGCCAGGGCGTGGTTCAG",
            "Marker": {
              "$type": "5",
              "Selected": true,
              "TestName": "MLST",
              "TypingTest": "Allelic",
              "Name": "SUCA",
              "ForwardPrimer": "",
              "ReversePrimer": "",
              "AmpliconSize": -1,
              "AmpliconRange": 0,
              "AllelicDatabaseFilename": "/home/peter/mist_cmd_wiki/MIST_Senterica_MLST_test/alleles/SUCA.fas",
              "RepeatSize": 0
            }
          },
          "THRA": {
            "$type": "3",
            "BlastResults": {
              "$type": "4",
              "QueryAln": "GTGCTGGGCCGTAATGGTTCCGACTATTCCGCCGCCGTGCTGGCCGCCTGTTTACGCGCTGACTGCTGTGAAATCTGGACTGACGTCGATGGCGTGTATACCTGTGACCCGCGCCAGGTGCCGGACGCCAGACTGCTGAAATCGATGTCCTACCAGGAAGCGATGGAACTCTCTTACTTCGGCGCCAAAGTCCTTCACCCTCGCACCATAACGCCTATCGCCCAGTTCCAGATCCCCTGTCTGATTAAAAATACCGGTAATCCGCAGGCGCCAGGAACGCTGATCGGCGCGTCCAGCGACGATGATAATCTGCCGGTTAAAGGGATCTCTAACCTTAACAACATGGCGATGTTTAGCGTCTCCGGCCCGGGAATGAAAGGGATGATTGGGATGGCGGCGCGTGTTTTCGCCGCCATGTCTCGCGCCGGGATCTCGGTGGTGCTCATTACCCAGTCCTCCTCTGAGTACAGCATCAGCTTCTGTGTGCCGCAGAGTGACTGC",
              "SubjAln": "GTGCTGGGCCGTAATGGTTCCGACTATTCCGCCGCCGTGCTGGCCGCCTGTTTACGCGCTGACTGCTGTGAAATCTGGACTGACGTCGATGGCGTGTATACCTGTGACCCGCGCCAGGTGCCGGACGCCAGGCTGCTGAAATCGATGTCCTACCAGGAAGCGATGGAACTCTCTTACTTCGGCGCCAAAGTCCTTCACCCTCGCACCATTACGCCCATCGCCCAGTTCCAGATCCCCTGTCTGATTAAAAATACCGGTAATCCGCAGGCGCCAGGAACGCTGATCGGCGCGTCCAGCGACGATGATAACCTGCCGGTTAAAGGGATCTCTAACCTTAACAACATGGCGATGTTTAGCGTCTCCGGCCCGGGAATGAAAGGGATGATTGGGATGGCGGCGCGTGTTTTCGCCGCCATGTCTCGCGCCGGGATCTCGGTGGTGCTCATTACCCAGTCCTCCTCTGAGTACAGCATCAGTTTCTGTGTGCCGCAGAGTGACTGC",
              "Gaps": 0,
              "QueryName": "0",
              "SubjectName": "0",
              "PercentIdentity": 99,
              "SubjectStartIndex": 936,
              "SubjectEndIndex": 1436,
              "ReverseComplement": false,
              "AlignmentLength": 501,
              "BitScore": 881,
              "QueryLength": 501,
              "QueryEndIndex": 501,
              "QueryStartIndex": 1
            },
            "ForwardPrimerBlastResult": null,
            "ReversePrimerBlastResult": null,
            "StrainName": "Se_Typhimurium_LT2",
            "MarkerName": "THRA",
            "TestName": "MLST",
            "MarkerCall": "2",
            "AlleleMatch": "THRA2",
            "ContigMatchName": "NC_003197_Salmonella_enterica_subsp._enterica_serovar_Typhimurium_str._LT2_chromosome,_complete_genome.",
            "Mismatches": 0,
            "BlastPercentIdentity": 99,
            "BlastAlignmentLength": 501,
            "BlastGaps": 0,
            "AmpliconSize": 501,
            "CorrectMarkerMatch": true,
            "StartIndex": 936,
            "EndIndex": 1436,
            "Amplicon": "GTGCTGGGCCGTAATGGTTCCGACTATTCCGCCGCCGTGCTGGCCGCCTGTTTACGCGCTGACTGCTGTGAAATCTGGACTGACGTCGATGGCGTGTATACCTGTGACCCGCGCCAGGTGCCGGACGCCAGGCTGCTGAAATCGATGTCCTACCAGGAAGCGATGGAACTCTCTTACTTCGGCGCCAAAGTCCTTCACCCTCGCACCATTACGCCCATCGCCCAGTTCCAGATCCCCTGTCTGATTAAAAATACCGGTAATCCGCAGGCGCCAGGAACGCTGATCGGCGCGTCCAGCGACGATGATAACCTGCCGGTTAAAGGGATCTCTAACCTTAACAACATGGCGATGTTTAGCGTCTCCGGCCCGGGAATGAAAGGGATGATTGGGATGGCGGCGCGTGTTTTCGCCGCCATGTCTCGCGCCGGGATCTCGGTGGTGCTCATTACCCAGTCCTCCTCTGAGTACAGCATCAGTTTCTGTGTGCCGCAGAGTGACTGC",
            "Marker": {
              "$type": "5",
              "Selected": true,
              "TestName": "MLST",
              "TypingTest": "Allelic",
              "Name": "THRA",
              "ForwardPrimer": "",
              "ReversePrimer": "",
              "AmpliconSize": -1,
              "AmpliconRange": 0,
              "AllelicDatabaseFilename": "/home/peter/mist_cmd_wiki/MIST_Senterica_MLST_test/alleles/THRA.fas",
              "RepeatSize": 0
            }
          },
          "AROC": {
            "$type": "3",
            "BlastResults": {
              "$type": "4",
              "QueryAln": "GTTTTTCGCCCGGGACACGCGGATTACACCTATGAGCAGAAATACGGCCTGCGCGATTACCGCGGCGGTGGACGTTCTTCCGCGCGTGAAACCGCGATGCGCGTAGCGGCAGGGGCGATCGCCAAGAAATACTTGGCGGAAAAGTTCGGCATCGAAATCCGCGGCTGCCTGACCCAGATGGGCGACATTCCGCTGGAGATTAAAGACTGGCGTCAGGTTGAGCTTAATCCGTTCTTTTGCCCCGATGCGGACAAACTTGACGCGCTGGACGAACTGATGCGCGCGCTGAAAAAAGAGGGTGACTCCATCGGCGCGAAAGTGACGGTGATGGCGAGCGGCGTGCCGGCAGGGCTTGGCGAACCGGTATTTGACCGACTGGATGCGGACATCGCCCATGCGCTGATGAGCATCAATGCGGTGAAAGGCGTGGAGATCGGCGAAGGATTTAACGTGGTGGCGCTGCGCGGCAGCCAGAATCGCGATGAAATCACGGCGCAGGGT",
              "SubjAln": "GTTTTTCGTCCGGGACACGCGGATTACACCTATGAGCAGAAATACGGCCTGCGCGATTACCGTGGCGGTGGACGTTCTTCCGCGCGTGAAACCGCGATGCGCGTAGCGGCAGGGGCGATCGCCAAGAAATACCTGGCGGAAAAGTTCGGCATCGAAATCCGCGGCTGCCTGACCCAGATGGGCGACATTCCGCTGGAGATTAAAGACTGGCGTCAGGTTGAGCTTAATCCGTTCTTTTGTCCCGATGCGGACAAACTTGACGCGCTGGACGAACTGATGCGCGCGCTGAAAAAAGAGGGTGACTCCATCGGCGCGAAAGTGACGGTGATGGCGAGCGGCGTGCCGGCAGGGCTTGGCGAACCGGTATTTGACCGACTGGATGCGGACATCGCCCATGCGCTGATGAGCATTAATGCGGTGAAAGGCGTGGAGATCGGCGAAGGATTTAACGTGGTGGCGCTGCGCGGCAGCCAGAATCGCGATGAAATCACGGCGCAGGGT",
              "Gaps": 0,
              "QueryName": "0",
              "SubjectName": "0",
              "PercentIdentity": 99,
              "SubjectStartIndex": 2494825,
              "SubjectEndIndex": 2495325,
              "ReverseComplement": true,
              "AlignmentLength": 501,
              "BitScore": 881,
              "QueryLength": 501,
              "QueryEndIndex": 501,
              "QueryStartIndex": 1
            },
            "ForwardPrimerBlastResult": null,
            "ReversePrimerBlastResult": null,
            "StrainName": "Se_Typhimurium_LT2",
            "MarkerName": "AROC",
            "TestName": "MLST",
            "MarkerCall": "10",
            "AlleleMatch": "AROC10",
            "ContigMatchName": "NC_003197_Salmonella_enterica_subsp._enterica_serovar_Typhimurium_str._LT2_chromosome,_complete_genome.",
            "Mismatches": 0,
            "BlastPercentIdentity": 99,
            "BlastAlignmentLength": 501,
            "BlastGaps": 0,
            "AmpliconSize": 501,
            "CorrectMarkerMatch": true,
            "StartIndex": 2494825,
            "EndIndex": 2495325,
            "Amplicon": "GTTTTTCGTCCGGGACACGCGGATTACACCTATGAGCAGAAATACGGCCTGCGCGATTACCGTGGCGGTGGACGTTCTTCCGCGCGTGAAACCGCGATGCGCGTAGCGGCAGGGGCGATCGCCAAGAAATACCTGGCGGAAAAGTTCGGCATCGAAATCCGCGGCTGCCTGACCCAGATGGGCGACATTCCGCTGGAGATTAAAGACTGGCGTCAGGTTGAGCTTAATCCGTTCTTTTGTCCCGATGCGGACAAACTTGACGCGCTGGACGAACTGATGCGCGCGCTGAAAAAAGAGGGTGACTCCATCGGCGCGAAAGTGACGGTGATGGCGAGCGGCGTGCCGGCAGGGCTTGGCGAACCGGTATTTGACCGACTGGATGCGGACATCGCCCATGCGCTGATGAGCATTAATGCGGTGAAAGGCGTGGAGATCGGCGAAGGATTTAACGTGGTGGCGCTGCGCGGCAGCCAGAATCGCGATGAAATCACGGCGCAGGGT",
            "Marker": {
              "$type": "5",
              "Selected": true,
              "TestName": "MLST",
              "TypingTest": "Allelic",
              "Name": "AROC",
              "ForwardPrimer": "",
              "ReversePrimer": "",
              "AmpliconSize": -1,
              "AmpliconRange": 0,
              "AllelicDatabaseFilename": "/home/peter/mist_cmd_wiki/MIST_Senterica_MLST_test/alleles/AROC.fas",
              "RepeatSize": 0
            }
          },
          "DNAN": {
            "$type": "3",
            "BlastResults": {
              "$type": "4",
              "QueryAln": "ATGGAGATGGTCGCGCGCGTTACGCTTTCTCAGCCGCATGAGCCAGGCGCCACTACCGTGCCGGCGCGGAAATTCTTTGATATCTGCCGCGGCCTGCCGGAGGGCGCGGAGATTGCCGTTCAGTTGGAAGGCGATCGGATGCTGGTGCGTTCTGGCCGTAGCCGCTTCTCGCTGTCTACGCTGCCTGCCGCCGATTTCCCGAATCTTGACGACTGGCAAAGCGAAGTTGAATTTACGCTGCCGCAGGCCACGATGAAGCGCCTGATTGAAGCGACCCAGTTTTCGATGGCCCATCAGGATGTGCGCTACTACTTAAACGGTATGCTGTTTGAAACGGAAGGTAGCGAACTGCGCACTGTTGCGACCGACGGCCACCGTCTGGCGGTGTGCTCAATGCCGCTGGAGGCGTCTTTACCTAGCCACTCGGTGATTGTGCCGCGTAAAGGCGTGATTGAACTGATGCGTATGCTCGACGGTGGCGAAAACCCGCTGCGCGTGCAG",
              "SubjAln": "ATGGAGATGGTCGCGCGCGTTACGCTTTCTCAGCCGCATGAGCCAGGCGCCACTACCGTGCCGGCGCGGAAATTCTTTGATATCTGCCGCGGCCTGCCGGAGGGCGCGGAGATTGCCGTTCAGTTGGAAGGCGATCGGATGCTGGTGCGTTCTGGCCGTAGCCGCTTCTCGCTGTCTACGCTGCCTGCCGCCGATTTCCCGAATCTTGACGACTGGCAAAGCGAAGTTGAATTTACGCTGCCGCAGGCCACGATGAAGCGCCTGATTGAATCGACCCAGTTTTCGATGGCCCATCAGGATGTGCGCTACTACTTAAACGGTATGCTGTTTGAAACGGAAGGTAGCGAACTGCGCACTGTCGCGACCGACGGCCACCGTCTGGCGGTGTGCTCAATGCCGCTGGAAGCGTCTTTACCCAGCCACTCGGTGATTGTGCCGCGTAAAGGCGTGATTGAACTGATGCGTATGCTCGACGGCGGCGAAAACCCGCTGCGCGTGCAG",
              "Gaps": 0,
              "QueryName": "0",
              "SubjectName": "0",
              "PercentIdentity": 99,
              "SubjectStartIndex": 4042968,
              "SubjectEndIndex": 4043468,
              "ReverseComplement": true,
              "AlignmentLength": 501,
              "BitScore": 881,
              "QueryLength": 501,
              "QueryEndIndex": 501,
              "QueryStartIndex": 1
            },
            "ForwardPrimerBlastResult": null,
            "ReversePrimerBlastResult": null,
            "StrainName": "Se_Typhimurium_LT2",
            "MarkerName": "DNAN",
            "TestName": "MLST",
            "MarkerCall": "7",
            "AlleleMatch": "DNAN7",
            "ContigMatchName": "NC_003197_Salmonella_enterica_subsp._enterica_serovar_Typhimurium_str._LT2_chromosome,_complete_genome.",
            "Mismatches": 0,
            "BlastPercentIdentity": 99,
            "BlastAlignmentLength": 501,
            "BlastGaps": 0,
            "AmpliconSize": 501,
            "CorrectMarkerMatch": true,
            "StartIndex": 4042968,
            "EndIndex": 4043468,
            "Amplicon": "ATGGAGATGGTCGCGCGCGTTACGCTTTCTCAGCCGCATGAGCCAGGCGCCACTACCGTGCCGGCGCGGAAATTCTTTGATATCTGCCGCGGCCTGCCGGAGGGCGCGGAGATTGCCGTTCAGTTGGAAGGCGATCGGATGCTGGTGCGTTCTGGCCGTAGCCGCTTCTCGCTGTCTACGCTGCCTGCCGCCGATTTCCCGAATCTTGACGACTGGCAAAGCGAAGTTGAATTTACGCTGCCGCAGGCCACGATGAAGCGCCTGATTGAATCGACCCAGTTTTCGATGGCCCATCAGGATGTGCGCTACTACTTAAACGGTATGCTGTTTGAAACGGAAGGTAGCGAACTGCGCACTGTCGCGACCGACGGCCACCGTCTGGCGGTGTGCTCAATGCCGCTGGAAGCGTCTTTACCCAGCCACTCGGTGATTGTGCCGCGTAAAGGCGTGATTGAACTGATGCGTATGCTCGACGGCGGCGAAAACCCGCTGCGCGTGCAG",
            "Marker": {
              "$type": "5",
              "Selected": true,
              "TestName": "MLST",
              "TypingTest": "Allelic",
              "Name": "DNAN",
              "ForwardPrimer": "",
              "ReversePrimer": "",
              "AmpliconSize": -1,
              "AmpliconRange": 0,
              "AllelicDatabaseFilename": "/home/peter/mist_cmd_wiki/MIST_Senterica_MLST_test/alleles/DNAN.fas",
              "RepeatSize": 0
            }
          },
          "HEMD": {
            "$type": "3",
            "BlastResults": {
              "$type": "4",
              "QueryAln": "GCAACGCTGACGGAAAACGATCTGGTTTTTGCCCTTTCACAGCACTCCGTCGCCTTTGCTCACGCCCAGCTCCAGCGGGATGGACGAAACTGGCCTGCGTCGCCGCGCTATTTCTCGATTGGCCGCACTACGGCGCTCGCCCTTCATACCGTTAGCGGGTTCGATATTCGTTATCCATTGGATCGGGAAATCAGCGAAGCCTTGCTACAATTACCTGAATTACAAAATATTGCGGGCAAACGCGCGCTGATTTTGCGTGGCAATGGCGGCCGCGAACTGCTGGGCGAAACCCTGACAGTTCGCGGAGCCGAAGTCAGTTTTTGTGAATGTTATCAACGATGTGCGAAACATTACGATGGCGCGGAAGAAGCGATGCGCTGGCATACTCGCGGCGTAACAACGCTTGTTGTTACCAGCGGCGAGATGTTGCAA",
              "SubjAln": "GCGACGCTGACGGAAAACGATCTGGTTTTTGCCCTTTCACAGCACGCTGTCGCCTTTGCTCACGCCCAGCTCCAGCGGGATGGTCGAAACTGGCCTGTGGCGCCGCGCTATTTCGCGATTGGCCGCACCACGGCGCTCGCCCTTCATACCGTTAGCGGGTTCGATATTCGTTATCCATTGGATCGGGAAATCAGCGAAGCCTTGCTACAATTACCTGAATTACAAAATATTGCGGGCAAACGCGCGCTGATTTTGCGTGGCAATGGCGGCCGCGAACTGCTGGGCGAAACCCTGACAGCGCGCGGAGCCGAAGTCAGTTTTTGTGAATGTTATCAACGATGTGCGAAACATTACGATGGCGCGGAAGAAGCGATGCGCTGGCATACTCGCGGCGTAACAACGCTTGTTGTTACCAGCGGCGAGATGTTGCAA",
              "Gaps": 0,
              "QueryName": "0",
              "SubjectName": "0",
              "PercentIdentity": 97.69,
              "SubjectStartIndex": 4144479,
              "SubjectEndIndex": 4144910,
              "ReverseComplement": true,
              "AlignmentLength": 432,
              "BitScore": 735,
              "QueryLength": 432,
              "QueryEndIndex": 432,
              "QueryStartIndex": 1
            },
            "ForwardPrimerBlastResult": null,
            "ReversePrimerBlastResult": null,
            "StrainName": "Se_Typhimurium_LT2",
            "MarkerName": "HEMD",
            "TestName": "MLST",
            "MarkerCall": "12",
            "AlleleMatch": "HEMD12",
            "ContigMatchName": "NC_003197_Salmonella_enterica_subsp._enterica_serovar_Typhimurium_str._LT2_chromosome,_complete_genome.",
            "Mismatches": 0,
            "BlastPercentIdentity": 97.69,
            "BlastAlignmentLength": 432,
            "BlastGaps": 0,
            "AmpliconSize": 432,
            "CorrectMarkerMatch": true,
            "StartIndex": 4144479,
            "EndIndex": 4144910,
            "Amplicon": "GCGACGCTGACGGAAAACGATCTGGTTTTTGCCCTTTCACAGCACGCTGTCGCCTTTGCTCACGCCCAGCTCCAGCGGGATGGTCGAAACTGGCCTGTGGCGCCGCGCTATTTCGCGATTGGCCGCACCACGGCGCTCGCCCTTCATACCGTTAGCGGGTTCGATATTCGTTATCCATTGGATCGGGAAATCAGCGAAGCCTTGCTACAATTACCTGAATTACAAAATATTGCGGGCAAACGCGCGCTGATTTTGCGTGGCAATGGCGGCCGCGAACTGCTGGGCGAAACCCTGACAGCGCGCGGAGCCGAAGTCAGTTTTTGTGAATGTTATCAACGATGTGCGAAACATTACGATGGCGCGGAAGAAGCGATGCGCTGGCATACTCGCGGCGTAACAACGCTTGTTGTTACCAGCGGCGAGATGTTGCAA",
            "Marker": {
              "$type": "5",
              "Selected": true,
              "TestName": "MLST",
              "TypingTest": "Allelic",
              "Name": "HEMD",
              "ForwardPrimer": "",
              "ReversePrimer": "",
              "AmpliconSize": -1,
              "AmpliconRange": 0,
              "AllelicDatabaseFilename": "/home/peter/mist_cmd_wiki/MIST_Senterica_MLST_test/alleles/HEMD.fas",
              "RepeatSize": 0
            }
          },
          "HISD": {
            "$type": "3",
            "BlastResults": {
              "$type": "4",
              "QueryAln": "ATTGCGGGATGCCAGAAGGTGGTTCTGTGCTCGCCGCCACCCATCGCTGATGAAATCCTCTATGCGGCGCAACTGTGTGGCGTGCAGGAAATCTTTAACGTCGGCGGCGCGCAGGCGATTGCCGCTCTGGCCTTCGGCAGCGAGTCCGTACCGAAAGTGGATAAAATTTTTGGCCCCGGCAACGCCTTTGTAACCGAAGCCAAGCGTCAGGTCAGCCAGCGTCTCGACGGCGCGGCTATCGATATGCCAGCCGGGCCGTCTGAAGTGCTGGTGATCGCCGACAGCGGCGCAACACCGGATTTCGTCGCTTCTGACCTGCTCTCCCAGGCTGAGCACGGCCCGGATTCCCAGGTGATCCTGCTGACGCCGGATGCTGACATTGCCCGCAAGGTGGCGGAGGCGGTAGAACGTCAACTGGCGGAACTGCCGCGCGCGGGCACCGCCCGGCAGGCCCTGAGCGCCAGTCGTCTGATTGTGACCAAAGATTTAGCGCAGTGCGTC",
              "SubjAln": "ATTGCGGGATGCCAGAAGGTGGTTCTGTGCTCGCCGCCGCCCATCGCTGATGAAATCCTCTATGCGGCGCAACTGTGTGGCGTGCAGGAAATCTTTAACGTCGGCGGCGCGCAGGCGATTGCCGCTCTGGCCTTCGGCAGCGAGTCCGTACCGAAAGTGGATAAAATTTTTGGCCCCGGCAACGCCTTTGTAACCGAAGCCAAACGTCAGGTCAGCCAGCGTCTCGACGGCGCGGCTATCGATATGCCAGCCGGGCCGTCTGAAGTACTGGTGATCGCAGACAGCGGCGCAACACCGGATTTCGTCGCTTCTGACCTGCTCTCCCAGGCTGAGCACGGCCCGGATTCCCAGGTGATCCTGCTGACGCCTGATGCTGACATTGCCCGCAAGGTGGCGGAGGCGGTAGAACGTCAACTGGCGGAACTGCCGCGCGCGGACACCGCCCGGCAGGCCCTGAGCGCCAGTCGTCTGATTGTGACCAAAGATTTAGCGCAGTGCGTC",
              "Gaps": 0,
              "QueryName": "0",
              "SubjectName": "0",
              "PercentIdentity": 98.8,
              "SubjectStartIndex": 2151066,
              "SubjectEndIndex": 2151566,
              "ReverseComplement": false,
              "AlignmentLength": 501,
              "BitScore": 877,
              "QueryLength": 501,
              "QueryEndIndex": 501,
              "QueryStartIndex": 1
            },
            "ForwardPrimerBlastResult": null,
            "ReversePrimerBlastResult": null,
            "StrainName": "Se_Typhimurium_LT2",
            "MarkerName": "HISD",
            "TestName": "MLST",
            "MarkerCall": "9",
            "AlleleMatch": "HISD9",
            "ContigMatchName": "NC_003197_Salmonella_enterica_subsp._enterica_serovar_Typhimurium_str._LT2_chromosome,_complete_genome.",
            "Mismatches": 0,
            "BlastPercentIdentity": 98.8,
            "BlastAlignmentLength": 501,
            "BlastGaps": 0,
            "AmpliconSize": 501,
            "CorrectMarkerMatch": true,
            "StartIndex": 2151066,
            "EndIndex": 2151566,
            "Amplicon": "ATTGCGGGATGCCAGAAGGTGGTTCTGTGCTCGCCGCCGCCCATCGCTGATGAAATCCTCTATGCGGCGCAACTGTGTGGCGTGCAGGAAATCTTTAACGTCGGCGGCGCGCAGGCGATTGCCGCTCTGGCCTTCGGCAGCGAGTCCGTACCGAAAGTGGATAAAATTTTTGGCCCCGGCAACGCCTTTGTAACCGAAGCCAAACGTCAGGTCAGCCAGCGTCTCGACGGCGCGGCTATCGATATGCCAGCCGGGCCGTCTGAAGTACTGGTGATCGCAGACAGCGGCGCAACACCGGATTTCGTCGCTTCTGACCTGCTCTCCCAGGCTGAGCACGGCCCGGATTCCCAGGTGATCCTGCTGACGCCTGATGCTGACATTGCCCGCAAGGTGGCGGAGGCGGTAGAACGTCAACTGGCGGAACTGCCGCGCGCGGACACCGCCCGGCAGGCCCTGAGCGCCAGTCGTCTGATTGTGACCAAAGATTTAGCGCAGTGCGTC",
            "Marker": {
              "$type": "5",
              "Selected": true,
              "TestName": "MLST",
              "TypingTest": "Allelic",
              "Name": "HISD",
              "ForwardPrimer": "",
              "ReversePrimer": "",
              "AmpliconSize": -1,
              "AmpliconRange": 0,
              "AllelicDatabaseFilename": "/home/peter/mist_cmd_wiki/MIST_Senterica_MLST_test/alleles/HISD.fas",
              "RepeatSize": 0
            }
          }
        }
      },
      "Metadata": {
        "MLST": [
          {
            "ST": "19",
            "EBG": "1",
            "SOURCE": "DVI",
            "REFSTRAIN": "9924828",
            "LINEAGE": ""
          }
        ]
      }
    }
  ],
  "TestTypes": {
    "MLST": "Allelic"
  },
  "TestMetadataFile": {
    "MLST": {
      "$type": "6",
      "FilePath": "/home/peter/mist_cmd_wiki/MIST_Senterica_MLST_test/MLST.txt",
      "TestName": "MLST",
      "Fields": [
        "ST",
        "EBG",
        "AROC",
        "DNAN",
        "HEMD",
        "HISD",
        "PURE",
        "SUCA",
        "THRA",
        "SOURCE",
        "REFSTRAIN",
        "LINEAGE"
      ],
      "MarkerIndices": [
        {
          "k": {
            "$type": "5",
            "Selected": true,
            "TestName": "MLST",
            "TypingTest": "Allelic",
            "Name": "PURE",
            "ForwardPrimer": "",
            "ReversePrimer": "",
            "AmpliconSize": -1,
            "AmpliconRange": 0,
            "AllelicDatabaseFilename": "/home/peter/mist_cmd_wiki/MIST_Senterica_MLST_test/alleles/PURE.fas",
            "RepeatSize": 0
          },
          "v": 6
        },
        {
          "k": {
            "$type": "5",
            "Selected": true,
            "TestName": "MLST",
            "TypingTest": "Allelic",
            "Name": "SUCA",
            "ForwardPrimer": "",
            "ReversePrimer": "",
            "AmpliconSize": -1,
            "AmpliconRange": 0,
            "AllelicDatabaseFilename": "/home/peter/mist_cmd_wiki/MIST_Senterica_MLST_test/alleles/SUCA.fas",
            "RepeatSize": 0
          },
          "v": 7
        },
        {
          "k": {
            "$type": "5",
            "Selected": true,
            "TestName": "MLST",
            "TypingTest": "Allelic",
            "Name": "THRA",
            "ForwardPrimer": "",
            "ReversePrimer": "",
            "AmpliconSize": -1,
            "AmpliconRange": 0,
            "AllelicDatabaseFilename": "/home/peter/mist_cmd_wiki/MIST_Senterica_MLST_test/alleles/THRA.fas",
            "RepeatSize": 0
          },
          "v": 8
        },
        {
          "k": {
            "$type": "5",
            "Selected": true,
            "TestName": "MLST",
            "TypingTest": "Allelic",
            "Name": "AROC",
            "ForwardPrimer": "",
            "ReversePrimer": "",
            "AmpliconSize": -1,
            "AmpliconRange": 0,
            "AllelicDatabaseFilename": "/home/peter/mist_cmd_wiki/MIST_Senterica_MLST_test/alleles/AROC.fas",
            "RepeatSize": 0
          },
          "v": 2
        },
        {
          "k": {
            "$type": "5",
            "Selected": true,
            "TestName": "MLST",
            "TypingTest": "Allelic",
            "Name": "DNAN",
            "ForwardPrimer": "",
            "ReversePrimer": "",
            "AmpliconSize": -1,
            "AmpliconRange": 0,
            "AllelicDatabaseFilename": "/home/peter/mist_cmd_wiki/MIST_Senterica_MLST_test/alleles/DNAN.fas",
            "RepeatSize": 0
          },
          "v": 3
        },
        {
          "k": {
            "$type": "5",
            "Selected": true,
            "TestName": "MLST",
            "TypingTest": "Allelic",
            "Name": "HEMD",
            "ForwardPrimer": "",
            "ReversePrimer": "",
            "AmpliconSize": -1,
            "AmpliconRange": 0,
            "AllelicDatabaseFilename": "/home/peter/mist_cmd_wiki/MIST_Senterica_MLST_test/alleles/HEMD.fas",
            "RepeatSize": 0
          },
          "v": 4
        },
        {
          "k": {
            "$type": "5",
            "Selected": true,
            "TestName": "MLST",
            "TypingTest": "Allelic",
            "Name": "HISD",
            "ForwardPrimer": "",
            "ReversePrimer": "",
            "AmpliconSize": -1,
            "AmpliconRange": 0,
            "AllelicDatabaseFilename": "/home/peter/mist_cmd_wiki/MIST_Senterica_MLST_test/alleles/HISD.fas",
            "RepeatSize": 0
          },
          "v": 5
        }
      ]
    }
  }
}
	|;

	my $jsonObj = JSON->new->allow_nonref;
	return $jsonObj->decode($providedJsonText);
}

sub _runMist{
	my $markerFile = shift;
	
	my $outFile = $markersDirectory . 'output.mist';

	#run MIST
	my $systemLine = 'mono ' . $mistDirectory
		. 'MIST.exe ' . ' -j ' 
		. $outFile. ' -c ' .  1
		. ' -t ' . $markerFile . ' -a ' . $markersDirectory . 'alleles/' . ' ' . $testFileName;

	print "running mist:\n$systemLine\n";
	system($systemLine);
	
	#get output with File::Slurp
	my $jsonText=read_file($outFile);
	
	#with JSON module
	my $jsonHashRef = from_json($jsonText);

	#delete temp file
	unlink $outFile;
	return $jsonHashRef;
}



sub _getMarkers{
	my $jsonHashRef =shift;

	#we will run everything as a separate test, therefor only a single test type
	my @testTypes = keys %{$jsonHashRef->{'TestTypes'}};

	if(scalar(@testTypes) != 1){
		print STDERR "More than one test type!\n";
		exit(1);
	}
	
	#get all the potential markers from the test
	my $allMarkersRef = _getAllMarkersForTest($jsonHashRef,$testTypes[0]);

	my @lowerCaseMarkers;
	foreach my $marker(@{$allMarkersRef}){
		#print "Marker: $marker\n";
		$marker = lc($marker);
		$marker =~ s/\W//g;
		push @lowerCaseMarkers, $marker;
	}
	return \@lowerCaseMarkers;
}



sub _createSchema{
	my $markersRef = shift;
	my $fileName = shift;

	#get name of schema
	my $schemaLarge;
	my $schemaSmall;

	foreach my $name(@schema){
		if($fileName =~ m/\Q$name\E/){
			$schemaLarge=$name;
			$schemaSmall=lc($schemaLarge);
			last;
		}
	}

	unless(defined $schemaSmall){
		die "Could not match $fileName to schema";
	}

	#get type of data
	my $dataType='text';
	# if($schemaSmall eq 'crispol' || $schemaSmall eq 'mlst' || $schemaSmall eq 'mlva'){
	# 	$dataType = 'numeric';
	# }
	# else{
	# 	$dataType = 'text';
	# }

my $text = q|#!/usr/bin/env perl

package STyper::Schema::Result::| . $schemaLarge . q|;

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../../../";
use parent qw/DBIx::Class::Core/;

__PACKAGE__->table('| . $schemaSmall . q|');
__PACKAGE__->add_columns(
	'strainid'=>{
			'data_type'=>'integer',
			'is_nullable'=>0
	},
	'verbose_output'=>{
		'data_type'=>'text',
		'is_nullable'=>1
	},|;

	foreach my $marker(@{$markersRef}){
		$text .= q|'| . $schemaSmall . '_' . $marker . q|'=>{
        'data_type'=>'| . $dataType . q|',
        'is_nullable'=>1
		},
		|;
	}

	$text .= q|
	); 
__PACKAGE__->belongs_to('strain'=>'STyper::Schema::Result::Strain','strainid');
__PACKAGE__->set_primary_key('strainid');
1;
	|;

	return($schemaLarge, $text);
}



sub _getAllMarkersForTest{
	my $jsonHashRef=shift;
	my $testName = shift;

	#Metadata and markers data are in different JSON levels
	#combine them as we need to report it all

	my @markers =keys %{$jsonHashRef->{'Results'}->[0]->{'TestResults'}->{$testName}};
	my @metaMarkers = keys %{$jsonHashRef->{'Results'}->[0]->{'Metadata'}->{$testName}->[0]};

	return [@markers,@metaMarkers];
}