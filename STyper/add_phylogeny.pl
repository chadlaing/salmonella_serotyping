#!/usr/bin/env perl

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../";
use Modules::PhylogenyManipulation;

#dbi:Pg:dbname=styper;host=localhost;port=5432','phac','phac123'
my $adder = Modules::PhylogenyManipulation->new();

#$adder->populateDirectory();
#$adder->addPanGenome('/home/chad/salmonella_genomes/Salmonella_enterica_serovar_Enteritidis_P125109_uid30687');
#$adder->runPanseq();
$adder->addTree('/home/chad/workspace/salmonella_serotyping/App/tree.new');
#$adder->createNewTree();
print "Phylogeny played with\n";