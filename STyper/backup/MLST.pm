#!/usr/bin/env/perl

package STyper::Schema::Result::MLST;

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../../../";
use parent qw/DBIx::Class::Core/;

#note refstrain and lineage should be of type text

__PACKAGE__->table('mlst');
__PACKAGE__->add_columns(
	'strainid'=>{
			'data_type'=>'integer',
			'is_nullable'=>0
	},
	'verbose_output'=>{
		'data_type'=>'text',
		'is_nullable'=>1
	},
	'mlst_aroc'=>{
	        'data_type'=>'numeric',
	        'is_nullable'=>1
	},
	'mlst_dnan'=>{
	        'data_type'=>'numeric',
	        'is_nullable'=>1
	},
	'mlst_hemd'=>{
	        'data_type'=>'numeric',
	        'is_nullable'=>1
	},
	'mlst_hisd'=>{
	        'data_type'=>'numeric',
	        'is_nullable'=>1
	},
	'mlst_pure'=>{
	        'data_type'=>'numeric',
	        'is_nullable'=>1
	},
	'mlst_suca'=>{
	        'data_type'=>'numeric',
	        'is_nullable'=>1
	},
	'mlst_thra'=>{
	        'data_type'=>'numeric',
	        'is_nullable'=>1
	},
	'mlst_st'=>{
	        'data_type'=>'numeric',
	        'is_nullable'=>1
	},
	'mlst_ebg'=>{
	        'data_type'=>'numeric',
	        'is_nullable'=>1
	},
	'mlst_source'=>{
	        'data_type'=>'text',
	        'is_nullable'=>1
	},
	'mlst_refstrain'=>{
	        'data_type'=>'text',
	        'is_nullable'=>1
	},
	'mlst_lineage'=>{
	        'data_type'=>'text',
	        'is_nullable'=>1
	}
); 
__PACKAGE__->belongs_to('strain'=>'STyper::Schema::Result::Strain','strainid');
__PACKAGE__->set_primary_key('strainid');
1;