#!/usr/bin/env perl

package STyper::Schema::Result::AMR;

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../../../";
use parent qw/DBIx::Class::Core/;

__PACKAGE__->table('amr');
__PACKAGE__->add_columns(
	'strainid'=>{
			'data_type'=>'integer',
			'is_nullable'=>0
	},
	'verbose_output'=>{
		'data_type'=>'text',
		'is_nullable'=>1
	},
	'amr_fimh'=>{
        'data_type'=>'numeric',
        'is_nullable'=>1
	},
	'amr_ssel'=>{
	        'data_type'=>'numeric',
	        'is_nullable'=>1
	},
	'amr_crispr1'=>{
	        'data_type'=>'numeric',
	        'is_nullable'=>1
	},
	'amr_crispr2'=>{
	        'data_type'=>'numeric',
	        'is_nullable'=>1
	}
); 
__PACKAGE__->belongs_to('strain'=>'STyper::Schema::Result::Strain','strainid');
__PACKAGE__->set_primary_key('strainid');
1;