#!/usr/bin/env/perl

package STyper::Schema::Result::Strain;

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../../../";
use parent qw/DBIx::Class::Core/;

__PACKAGE__->table('strain');
__PACKAGE__->add_columns(
	'strainid'=>{
			'data_type'=>'serial',
			'is_nullable'=>0
		},
	'name'=>{
			'data_type'=>'text',
			'is_nullable'=>1
		},
	'sequence'=>{
			'data_type'=>'text',
			'is_nullable'=>1
		},
	'gb'=>{
			'data_type'=>'text',
			'is_nullable'=>1
		},
	'filename'=>{
			'data_type'=>'text',
			'is_nullable'=>1
		},
	'intree'=>{
			'data_type'=>'boolean',
			'default_value'=>'FALSE',
			'is_nullable'=>1
	}
); 
__PACKAGE__->set_primary_key('strainid');

1;









