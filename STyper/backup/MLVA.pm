#!/usr/bin/env/perl

package STyper::Schema::Result::MLVA;

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../../../";
use parent qw/DBIx::Class::Core/;

__PACKAGE__->table('mlva');
__PACKAGE__->add_columns(
	'strainid'=>{
			'data_type'=>'integer',
			'is_nullable'=>0
	},
	'verbose_output'=>{
		'data_type'=>'text',
		'is_nullable'=>1
	},
	'mlva_se3'=>{
        'data_type'=>'numeric',
        'is_nullable'=>1
	},
	'mlva_se7'=>{
	        'data_type'=>'numeric',
	        'is_nullable'=>1
	},
	'mlva_sentr1'=>{
	        'data_type'=>'numeric',
	        'is_nullable'=>1
	},
	'mlva_sentr2'=>{
	        'data_type'=>'numeric',
	        'is_nullable'=>1
	},
	'mlva_sentr3'=>{
	        'data_type'=>'numeric',
	        'is_nullable'=>1
	},
	'mlva_sentr4'=>{
	        'data_type'=>'numeric',
	        'is_nullable'=>1
	},
	'mlva_sentr5'=>{
	        'data_type'=>'numeric',
	        'is_nullable'=>1
	},
	'mlva_sentr6'=>{
	        'data_type'=>'numeric',
	        'is_nullable'=>1
	},
	'mlva_sentr7'=>{
	        'data_type'=>'numeric',
	        'is_nullable'=>1
	}
); 
__PACKAGE__->belongs_to('strain'=>'STyper::Schema::Result::Strain','strainid');
__PACKAGE__->set_primary_key('strainid');
1;
