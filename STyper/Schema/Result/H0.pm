#!/usr/bin/env perl

package STyper::Schema::Result::H0;

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../../../";
use parent qw/DBIx::Class::Core/;

__PACKAGE__->table('h0');
__PACKAGE__->add_columns(
	'strainid'=>{
			'data_type'=>'integer',
			'is_nullable'=>0
	},
	'verbose_output'=>{
		'data_type'=>'text',
		'is_nullable'=>1
	},'h0_104108_enxz15'=>{
        'data_type'=>'text',
        'is_nullable'=>1
		},
		'h0_065lv__sub_iii'=>{
        'data_type'=>'text',
        'is_nullable'=>1
		},
		'h0_084z6_subii'=>{
        'data_type'=>'text',
        'is_nullable'=>1
		},
		'h0_106107_enz15andenxz15'=>{
        'data_type'=>'text',
        'is_nullable'=>1
		},
		'h0_083z6'=>{
        'data_type'=>'text',
        'is_nullable'=>1
		},
		'h0_091116_z6andz672'=>{
        'data_type'=>'text',
        'is_nullable'=>1
		},
		'h0_120encomp2'=>{
        'data_type'=>'text',
        'is_nullable'=>1
		},
		'h0_068114_lw'=>{
        'data_type'=>'text',
        'is_nullable'=>1
		},
		'h0_105106_enx4'=>{
        'data_type'=>'text',
        'is_nullable'=>1
		},
		'h0_064lcomp'=>{
        'data_type'=>'text',
        'is_nullable'=>1
		},
		'h0_119encomp1'=>{
        'data_type'=>'text',
        'is_nullable'=>1
		},
		'h0_06761_lz13_lv'=>{
        'data_type'=>'text',
        'is_nullable'=>1
		},
		'h0_h0'=>{
        'data_type'=>'text',
        'is_nullable'=>1
		},
		
	); 
__PACKAGE__->belongs_to('strain'=>'STyper::Schema::Result::Strain','strainid');
__PACKAGE__->set_primary_key('strainid');
1;
	