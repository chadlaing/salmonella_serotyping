#!/usr/bin/env perl

package STyper::Schema::Result::Tree;

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../../../";
use parent qw/DBIx::Class::Core/;

__PACKAGE__->table('tree');
__PACKAGE__->add_columns(
	'treeid'=>{
			'data_type'=>'serial',
			'is_nullable'=>0
		},
	'tree'=>{
			'data_type'=>'text',
			'is_nullable'=>1
		},
	'pangenome'=>{
		'data_type'=>'text',
		'is_nullable'=>1
	},
	'alignment'=>{
		'data_type'=>'text',
		'is_nullable'=>1
	},
); 
__PACKAGE__->set_primary_key('treeid');

1;