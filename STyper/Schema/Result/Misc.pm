#!/usr/bin/env perl

package STyper::Schema::Result::Misc;

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../../../";
use parent qw/DBIx::Class::Core/;

__PACKAGE__->table('misc');
__PACKAGE__->add_columns(
	'strainid'=>{
			'data_type'=>'integer',
			'is_nullable'=>0
	},
	'verbose_output'=>{
		'data_type'=>'text',
		'is_nullable'=>1
	},'misc_122sdf1_serovar_specific'=>{
        'data_type'=>'text',
        'is_nullable'=>1
		},
		'misc_111306_entrfbe'=>{
        'data_type'=>'text',
        'is_nullable'=>1
		},
		'misc_116210_pullorum1'=>{
        'data_type'=>'text',
        'is_nullable'=>1
		},
		'misc_121enteritidis_dublin_serovar_specific'=>{
        'data_type'=>'text',
        'is_nullable'=>1
		},
		'misc_109293_dublinnupc'=>{
        'data_type'=>'text',
        'is_nullable'=>1
		},
		'misc_112286_entshda'=>{
        'data_type'=>'text',
        'is_nullable'=>1
		},
		'misc_113120_inva'=>{
        'data_type'=>'text',
        'is_nullable'=>1
		},
		'misc_114308_pept'=>{
        'data_type'=>'text',
        'is_nullable'=>1
		},
		'misc_115310_pept3'=>{
        'data_type'=>'text',
        'is_nullable'=>1
		},
		'misc_110213_enteritidis1'=>{
        'data_type'=>'text',
        'is_nullable'=>1
		},
		'misc_117137_rhsgallinarum2'=>{
        'data_type'=>'text',
        'is_nullable'=>1
		},
		'misc_miscmarkers'=>{
        'data_type'=>'text',
        'is_nullable'=>1
		},
		
	); 
__PACKAGE__->belongs_to('strain'=>'STyper::Schema::Result::Strain','strainid');
__PACKAGE__->set_primary_key('strainid');
1;
	