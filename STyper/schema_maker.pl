#!/usr/bin/env/perl

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../";

#prints out the properly formatted schema for the data
#adds the prefix if required
my $prefix="antigen_";

my $dataString= '
001-1_A	002-140_B-3	003-141_B-4	004-4_C1	005-5_C2/C3	006-142_D1/D2-3	007-237_D1/D2-5	008-147_E1-2	009-9_G	010-10_H	011-12_J	012-13_K	013-14_L	014-16_M	015-18_N	016-N_wbdO_1_O30	017-19_0	018-21_P	019-23_S	020-S_rfaG_1_O41	021-24_V	022-26_Y	023-28_O:58	024-29_O:61	025-122_Vi	026-161_a-3	027-a-1_H1	028-a-2_H1	029-168_b-5	030-33_c	031-35_d-2	032-264_d-4	033-36_e,h	034-39_f sub 1-2	035-40_f,g	036-45_f,g,m,t	037-46_f,g,m,t-2	038-44_f,g,s	039-42_f,g,t-2	040-157_fgt,fg,gmt	041-245_fgt-gmt-mptu-mt	042-G-comp	043-183_g,m,q or g,q-2	044-173_g,m,s-4 (g,m[p]s)	045-g,m,t_H1	046-g,m,t-2_H1	047-190_g,p_g.p.s	048-56_g,s,t or g,t	049-57_g,z51	050-250_gmq-gq-5	051-219_gp-1	052-248_gp-2	053-249_gp-3	054-241_gpu-2	055-242_gpu-gp-fg-gst	056-g,q_H1	057-g,q-2_H1	058-g,q-3_H1	059-g,q-4_H1	060-252_gt-1	061-216_i-2	062-59_k	063-224_O:61-k1	064-L-comp	065-l,v__sub_III	066-179_l,z13,z28-2 (FliC)	067-61_l,z13_l,v	068-114_l,w	069-64_l,z28	070-mptu_H1	071-188_m,t_g,m,t	072-152_m,t_mtpu_gmt	073-153_m,t_mtpu_gmt-2	074-154_m,t_mtpu_gmt-3	075-69_r,[i]	076-283_r,[i]-4	077-t	078-t-1	079-169_y-2	080-170_y-3	081-71_z	082-z4-comp	083-z6	084-z6_subII	085-73_z10	086-74_z29	087-z36_JM	088-75_z38	089-76_z4,z23	090-78_z4,z24	091-116_z6 and z67-2	092-139_1,2	093-272_1,2,7-5	094-82_1,2_1,5_1,2,7	095-93_1,5	096-273_1,5,7	097-1,5,7_IIIb_H2	098-88_1,5_1,2,7	099-94_1,5-2	100-96_1,5-4	101-87_1,6	102-86_1,7	103-83_1,7_Indiana	104-108_e,n,x,z15	105-106_e,n,x-4	106-107_e,n,z15 and e,n,x,z15	107-223_Kottbus(1,5)	108-180_l,z13,z28-2 (FljB)	109-293_Dublin-nupC	110-213_Enteritidis-1	111-306_Ent-rfbE	112-286_Ent-shdA	113-120_invA	114-308_pepT	115-310_pepT-3	116-210_Pullorum-1	117-137_RHS-Gallinarum-2	118-1-comp	119-EN-comp-1	120-EN-comp-2	121-Enteritidis,_Dublin_serovar_specific	122-sdf-1_serovar_specific
';

$dataString =~ s/\R//g;
my @data = split('\t',$dataString);
#format as follows
#'strainid'=>{
#	'data_type'=>'numeric',
#	'is_nullable'=>1
#}

foreach my $datum(@data){
	my $lcDatum = lc($datum);
	$lcDatum =~ s/\W//g;
	
	my $output = qw/'/ . $prefix . $lcDatum . qw/'=>{/ . "\n\t" . qw/'data_type'=>'text',/ . "\n\t" . qw/'is_nullable'=>1/ . "\n},";
	print $output . "\n";
}








