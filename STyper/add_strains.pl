#!/usr/bin/env perl

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../";
use Modules::AddToDatabase;

#dbi:Pg:dbname=styper;host=localhost;port=5432','phac','phac123'
my $adder = Modules::AddToDatabase->new(
	'configFile'=>"$FindBin::Bin/../all_settings.config",
	'input'=>'/home/chad/genomes/salmonella/'
);
$adder->addToDatabase();

print "Added strain\n";