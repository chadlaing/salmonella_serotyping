#!/usr/bin/env perl

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../";
use Modules::MIST;

my $mister= Modules::MIST->new(
	'numberOfCores'=>'1',
	'assays'=>['mlst','serotyping','crispol','rrna','mlva','amr','virulotyping','antigen','serovar','serogroup','h1','h2','h0','misc'],
	'markerFiles'=>{
		'mlst'=>'MLST.markers',
		'mlva'=>'Malorny_MLVA_2008.markers',
		'crispol'=>'CRISPOL.markers',
		'amr'=>'AMR_CRISPR.markers',
		'antigen'=>'00_Antigen_formula_assay.markers',
		'serovar'=>'01_Serovar_assay.markers',
		'serogroup'=>'02_Serogroup_assay.markers',
		'h1'=>'03_H1_assay.markers',
		'h2'=>'04_H2_assay.markers',
		'h0'=>'05_H0_assay.markers',
		'misc'=>'06_MiscMarkers_assay.markers'
	},
	'configFile'=>"$FindBin::Bin/../all_settings.config"
);

$mister->runMist();
