#!/usr/bin/perl
#add |lcl tag to all strains
#if have an lcl tag, keep
#if not, add an lcl tag using a number eg lcl|local_X|

use strict;
use warnings;
use IO::File;
use File::Copy;

my $directory = $ARGV[0];
opendir DIR, $directory;
my @strains = grep { $_ ne '.' && $_ ne '..' && $_ ne '.directory' } readdir DIR;
closedir DIR;
my $counter=0;

foreach my $strain(@strains){
	$counter++;
	my $strainFH = IO::File->new('<' . $directory. $strain) or die "Cannot open $directory$strain\n";
	my @strainLines = $strainFH->getlines();
	$strainFH->close;

	my $outFH = IO::File->new('>'. $directory.$strain . '_lcl') or die "$!\n";
	foreach my $line(@strainLines){
		if($line =~ m/^>/){
			if($line =~ m/^>lcl\|/){
				#nothing
			}
			else{
				$line =~ s/>/>lcl\|local_$counter\|/;
			}
		}
		$outFH->print($line);
	}
	$outFH->close();
}



	