BrfbJ-1_KF	BrfbJ-2_KF	C1wbaA-2_CY	C1wbaA-3_CY	C2rfbJ-1_KF	C2rfbJ-2_KF	DegenD1-rfbS-KF	Typhispecific-rfbS-KF	1,2-8_CY	e,n,x-1_CY	g,m-3_CY	g,m_new_PK	g,m_new2_PK	g,m_new3_PK	g,m_new4_PK	i-3_CY	i-8_JM	k-1_longer_CY	k-18_PK	k-8_JM	k-8_mod_PK	k-9_JM	r-1_CY	r-13_JM	r-13_mod_PK	r-17_JM	r-3_CY	r-4_mod_CY	z10-3_JM	z10-4_CY	z10-6_CY	InvA-A-RC_EW	Arab-1-40_CW	Arab-3_PK	O-group	Phase 1 H-antigen	Phase 2 H-antigen	invA	Typhi
Present																																		B				
	Present																																	B				
		Present																																C1				
			Present																															C1				
				Present																														C2				
					Present																													C2				
						Present																												D1				
							Present																															S. Typhi
								Present																												1,2		
									Present																											e,n,x		
										Present																									g,m			
											Present																								g,m			
												Present																							g,m			
													Present																						g,m			
														Present																					g,m			
															Present																				i			
																Present																			i			
																	Present																		k			
																		Present																	k			
																			Present																k			
																				Present															k			
																					Present														k			
																						Present													r			
																							Present												r			
																								Present											r			
																									Present										r			
																										Present									r			
																											Present								r			
																												Present							z10			
																													Present						z10			
																														Present					z10			
																															Present						invA	
