#apache2 
1234
To allow the .htaccess file to modify the links, you have to enable the rewrite mod.
This is done in Debian and OpenSUSE (maybe more) by the following:
	a2enmod rewrite
Then, the apache2 .conf file must also allow symlinks in the document root, as:
	Options FollowSymLinks
where the defualt is 
	Options none
Additionally in the apache2 .conf file, the :
	AllowOverride All 
must be enabled from
	AllowOverride None

AddType image/svg+xml svg
#if the above is not added, the svg will not show as an image!

In order to make Perl play nice:
mod_rewrite alters the PATH_INFO by turning it into a file system path,
so we repair it.
from https://metacpan.org/module/CGI::Application::Dispatch#DISPATCH-TABLE
$ENV{PATH_INFO} =~ s/^$ENV{DOCUMENT_ROOT}// if defined $ENV{PATH_INFO};

This will allow the auto-redirecting of links to their full pathname. For example
/home/d will auto redirect to /styper/dispatch.cgi/home/d but only display the /home/d in the addess bar.
This can all be customized in the .htaccess file
I have the system apache softlink to the styper.conf file in the App/ directory

Don't forget to restart apache2 eg.
	/etc/init.d/apache2 restart	
#

#Stain genome data
-All strains are stored in multi-fasta format in the database
-Plasmids are combined with their genomic sequences
-When taken out of the database, the strainid (primary key) is appended as lcl|strainid| to the
	front of all fasta headers of a given sequence
-We do not want to have the database sequences stored with this information



#setting up postgres-9.2 (from http://www.davidghedini.com/pg/entry/install_postgresql_9_on_centos)
-on the server, need to symlink pg_config etc. otherwise DBD::Pg will never compile

root@server1 [~]# ln -s /usr/pgsql-9.1/bin/pg_config /usr/bin  

Symlink 2: Symlink for the old data directory location of /var/lob/pgsql 

root@server1 [~]# ln -s /var/lib/pgsql/9.1/data /var/lib/pgsql    
root@server1 [~]# ln -s /var/lib/pgsql/9.1/backups /var/lib/pgsql    


#dbix::class when doing a deploy, add_drop_table=>1 will not work if there are no tables currently in the database.
# This needs to be 0 for an initial deploy.

#



