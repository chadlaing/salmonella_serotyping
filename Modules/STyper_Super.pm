#!/usr/bin/env perl
package Modules::STyper_Super;

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../";
use File::Basename;

use parent 'CGI::Application';
use CGI::Application::Plugin::Config::Simple;
use CGI::Application::Plugin::Redirect;
use CGI::Application::Plugin::Session;
use HTML::Template;
use Role::Tiny::With;
use Log::Log4perl qw/:easy/;

with 'Roles::Settings';

sub cgiapp_init{
	my $self = shift;

	#set paths
	#the template path is set using CGI::Application::Dispatch

	#get script location via File::Basename
	my $SCRIPT_LOCATION = dirname(__FILE__);
	
	#load config file and get settings
	#it is best to symlink a server specific file to this location
	$self->loadConfigFile("$SCRIPT_LOCATION/../all_settings.config");

	#Session information
	$self->session_config(DEFAULT_EXPIRY => '+8h');

 	#Initialize logging behaviour
    Log::Log4perl->init("$SCRIPT_LOCATION/../log4perl.conf");
	$self->logger(Log::Log4perl->get_logger());
	$self->logger->info("SISTR up and running");
}

sub logger{
	my $self=shift;
	$self->{'_logger'}=shift // return $self->{'_logger'};
}


1;

