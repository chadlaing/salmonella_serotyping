#!/usr/bin/env perl
package Modules::Upload;

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/..";
use Parallel::ForkManager;
use Modules::PhylogenyManipulation;
use Modules::AddToDatabase;
use Modules::MIST;
use parent 'Modules::STyper_Super';
use Role::Tiny::With;

#includes all the settings in the config file

with 'Roles::Database';

sub uploadedFiles{
        my $self=shift;
        $self->{'_uploadedFile'}=shift // return $self->{'_uploadedFile'};
}

sub setup{
	my $self=shift;

    $self->uploadedFiles([]);
	$self->start_mode('display');
	$self->run_modes(
		'display'=>'display',
		'upload'=>'upload',
                'finished'=>'finished'
	);

    unless(defined $self->dbixSchema){
        $self->connectDatabase();    
    }  
}

sub display{
	my $self=shift;

	my $template = $self->load_tmpl('upload.tmpl', die_on_bad_params=>0);
	return $template->output();
}

sub upload{
        my $self = shift;  

        #get the form data
        my $q = $self->query();
        
        #allow multiple uploads, so many files possible in the fileNames param
        my @fileNames = $q->param('fileNames');
        my @fileHandles = $q->upload('fileNames');

        foreach my $file($q->upload('fileNames')){
                my $cleanedFileName = $file;
                $cleanedFileName =~ s/[^\w\.-]/_/g;

                my $FH  = $file->handle;
                my $uploadedFile = $self->outputDirectory . $cleanedFileName;
                
                #add to the list of uploaded files
                push @{$self->uploadedFiles}, $uploadedFile;
                my $outputHandle = IO::File->new( '>' .  $uploadedFile) or die 
                        "Cannot create " . $uploadedFile;

                #upload file using 1024 byte buffer
                my $buffer;
                my $bytesread = $FH->read( $buffer, 1024 );
                while ($bytesread) {
                        $outputHandle->print($buffer);
                        $bytesread = $FH->read( $buffer, 1024 );
                }
                $outputHandle->close();
        }
        
        #redirect to the appropriate address, rather than displaying the template here
        #we want to run the update as a separate process in the background
        #redirect the user to the upload finished page
        #my $forkManager = Parallel::ForkManager->new(2);
       
        
        my $pid = fork();
        if(!defined $pid){
                die "cannot fork process!\n $!";
        };
         
        if($pid){
            #display alldone!   
            # close STDERR;
            # close STDIN;
            # close STDOUT;                         
            return $self->redirect('/styper/upload/f');
        }
        else{         
            $self->_processNewStrains();    
            exit(0);
        }
       
}

sub finished{
        my $self=shift;

        my $template = $self->load_tmpl('upload_finished.tmpl', die_on_bad_params=>0);
        return $template->output();
}

sub _processNewStrains{
        my $self=shift;

        my @newStrainIds;
        foreach my $newStrain(@{$self->uploadedFiles}){
            $self->logger->debug("newstrain: $newStrain");
                #add the strain to the database
                my $dbAdder = Modules::AddToDatabase->new(
                        'input'=>$newStrain,
                        'dbixSchema'=>$self->dbixSchema
                );
                $dbAdder->addToDatabase();

                #if the strain is already in the database, skip
                #all previously added strains are in the $dbAdder->duplicateStrains hashref
                if($dbAdder->duplicateStrain){
                        next;
                }

                #this can accommodate more than one strain (directory)
                #but we are doing this one strain at a time
                push @newStrainIds, $dbAdder->addedStrains->[0];
        }
        #run mist on the new strains
        $self->_runMist(\@newStrainIds); 

        # my $TEST_VALUE=[211];
        # $self->_runMist($TEST_VALUE);       
}


sub _runMist{
    my $self=shift;
    my $strainIds = shift;

    foreach my $strainId(@{$strainIds}){
            my $mister= Modules::MIST->new(
            'numberOfCores'=>'1',
            'assays'=>['mlst','crispol','mlva','antigen','serovar','serogroup','h1','h2','h0','misc'],
            'markerFiles'=>{
                'mlst'=>'MLST.markers',
                'mlva'=>'Malorny_MLVA_2008.markers',
                'crispol'=>'CRISPOL.markers',
                'antigen'=>'00_Antigen_formula_assay.markers',
                'serovar'=>'01_Serovar_assay.markers',
                'serogroup'=>'02_Serogroup_assay.markers',
                'h1'=>'03_H1_assay.markers',
                'h2'=>'04_H2_assay.markers',
                'h0'=>'05_H0_assay.markers',
                'misc'=>'06_MiscMarkers_assay.markers'
            },
            'configFile'=>"$FindBin::Bin/../../all_settings.config",
            'strain'=>$strainId,
        );
        $mister->runMist();
    }
}


1;