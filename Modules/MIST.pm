#!/usr/bin/env perl

package Modules::MIST;

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../";
use STyper::Schema;
use IO::File;
use File::Basename;
use JSON;
use File::Slurp;
use Role::Tiny::With;
use Log::Log4perl;

with 'Roles::Database';
with 'Roles::Settings';

#object creation
sub new {
	my ($class) = shift;
	my $self = {};
	bless( $self, $class );
	$self->_initialize(@_);
	return $self;
}

sub numberOfCores{
	my $self=shift;
	$self->{'__cores'}=shift // return $self->{'__cores'};
}

sub wordSize{
	my $self=shift;
	$self->{'_wordSize'}=shift // return $self->{'_wordSize'};
}

sub overwrite{
	my $self=shift;
	$self->{'_overwrite'}=shift // return $self->{'_overwrite'};
}

sub strain{
	my $self=shift;
	$self->{'_strain'}=shift // return $self->{'_strain'};
}

sub assays{
	my $self=shift;
	$self->{'_assays'}=shift // return $self->{'_assays'};
}

sub markerFiles{
	my $self=shift;
	$self->{'_markerFiles'}=shift // return $self->{'_markerFiles'};
}

sub logger{
	my $self=shift;
	$self->{'_logger'}=shift // return $self->{'_logger'};
}

sub _initialize{
	my $self=shift;

	#Define configuration
	$self->logger(Log::Log4perl->get_logger()); 
	$self->logger->info("Ininitializing MIST.pm");

	my %params = @_;

	$self->loadConfigFile($params{'configFile'});

	$self->numberOfCores($params{'numberOfCores'}) // $self->_required('numberOfCores');
    $self->wordSize($params{'wordSize'}) // $self->wordSize(7);
    $self->overwrite($params{'overwrite'}) // $self->overwrite(0);
    $self->strain($params{'strain'}) // $self->strain('all');
    $self->markerFiles($params{'markerFiles'}) // $self->_required('list of marker files as a hash ref');
    $self->assays($params{'assays'}) // $self->_required('list of assays');

    foreach my $assay(@{$self->assays}){
    	$self->logger->info("Assay to run: $assay");
    }
	
	#connect db
	$self->connectDatabase();
}

sub _required{
	my $self=shift;
	my $param=shift;

	$self->logger->fatal("The parameter $param" . ' is required in Modules::Mist->new()');
	exit(1);
}

sub runMist{
	my $self=shift;

	my $strain = $self->strain;
	$self->logger->info("Running MIST");
	#go through all strains in database
	#if a strain is missing a MIST type, run MIST, add to database
	#if type already present, skip, unlace a replace flag is set

	#specify a 'find' value, or update entire database	
	if($strain eq 'all'){		
		my $allStrains = $self->dbixSchema->resultset('Strain');
		#use the ol forker
		#my $forkManager = Parallel::ForkManager->new($self->numberOfCores);

		while(my $currentStrain = $allStrains->next){
			$self->logger->info("Current gb for MIST run is: " . $currentStrain->gb);

			#$forkManager->start and next;
				$self->_runMist($currentStrain);
			#$forkManager->finish();
		}
		#$forkManager->wait_all_children();
	}
	else{
		my $targetStrain = $self->dbixSchema->resultset('Strain')->find({strainid=>$strain});
		$self->logger->debug("Target strain for MIST run is:" . $targetStrain->gb . "\n");	
		$self->_runMist($targetStrain);
	}	
}

sub _runMist{
	my $self=shift;
	my $targetStrain=shift;

	$self->logger->debug('gb:' . $targetStrain->gb);
	$self->logger->debug('targetStrain:' . $targetStrain->name);
	
	#create sequence file
	#requires a unique name every time
	#otherwise collision with previously created databases from MIST
	my $timeString = localtime();
	$timeString =~ s/\W//g;
	
	my $seqFile =  $self->outputDirectory . 'mist_seq_' . $timeString . $targetStrain->strainid. '.fasta';
	my $seqFH = IO::File->new('>' . $seqFile) or die "Could not create $seqFile\n";
	$self->logger->debug("Seqfile loation: $seqFile");
	$seqFH->print($targetStrain->sequence);
	$seqFH->close();	
	
	#all the MIST assays are in the $self->assays arrayRef
	#we will loop through each, get the 'out' and 'verbose' file from each
	#and then parse the output based on the assay	
	foreach my $assay(sort @{$self->assays}){
			my $jsonHashRef = $self->_runMistAssay($assay,$seqFile);
			$self->_parseData($assay,$targetStrain, $jsonHashRef);		
	}
	unlink $seqFile;	
}

sub _runMistAssay{
	my $self=shift;

	my $assay =shift;
	my $seqFile = shift;
	
	$self->logger->debug("Assay: $assay");
	
	unless(defined $self->markerFiles->{$assay}){
		$self->logger->info("Skipping undefined assay $assay");
		return;
	}
	
	#my $verboseFile = $self->outputDirectory . 'verbose.mist';

	my $outFile = $self->outputDirectory . 'output_' . $assay . '.json';
	$self->logger->info("Creating output file: $outFile");
	
	#run MIST
	my $systemLine = 'mono ' . $self->mistDirectory 
		. 'MIST.exe ' . ' -j ' 
		. $outFile. ' -c ' .  $self->numberOfCores
		. ' -a ' . $self->assaysDirectory . 'alleles/'
		. ' -T ' . $self->outputDirectory
		. ' -t ' . $self->assaysDirectory . $self->markerFiles->{$assay} . ' ' . $seqFile;
	$self->logger->info("systemLine: $systemLine");
	my $systemReturn = eval {system($systemLine)};
	$self->logger->info("System returned: $systemReturn");

	unless($systemReturn == 0){
		$self->logger->fatal($@);
	}
	
	$self->logger->info("Finished running MIST assay $assay");
	#then slurp file for database
	#get output with File::Slurp
	my $outputJson = read_file($outFile) or $self->logger->logdie("Cannot open $outFile");
	
	#clean up
	#unlink $outFile;
	return from_json($outputJson);
}

sub _parseData{
	my $self=shift;

	my $assay = shift;
	my $targetStrain=shift;
	my $jsonHashRef=shift;
	
	$self->logger->debug("Parsing assay $assay on strain " . $targetStrain->name);
	#We need to specify the name of the schema file we created, which is not identical to the assay name
	#therefore map the assay name to the correct schema
	my %schema=(
		'amr'=>'AMR',
		'crispol'=>'CRISPOL',
		'mlst'=>'MLST',
		'mlva'=>'MLVA',
		'antigen'=>'Antigen',
		'serovar'=>'Serovar',
		'serogroup'=>'Serogroup',
		'h1'=>'H1',
		'h2'=>'H2',
		'h0'=>'H0',
		'misc'=>'Misc'
	);	
	
	unless($schema{$assay}){
		$self->logger->info("Unknown assay\n");
		return 1;
	}

	#we will run everything as a separate test, therefore only a single test type
	my @testTypes = keys %{$jsonHashRef->{'TestTypes'}};
	
	if(scalar(@testTypes) > 1){
		$self->logger->fatal("More than one test type!");
		foreach my $test(@testTypes){
			$self->logger->fatal("test:$test");
		}
		exit(1);
	}

	#get all the potential markers from the test
	my $allMarkersRef = $self->_getAllMarkersForTest($jsonHashRef,$testTypes[0]);

	my $assayRow = $self->dbixSchema->resultset($schema{$assay})->find_or_create(
		{
			'strainid'=>$targetStrain->strainid
		}
	);
	
	#name column can be excluded, start at 1
	foreach my $marker(@{$allMarkersRef}){	
		$self->logger->info("Marker: $marker");	
		#the actual data for a marker is stored as follows:
		my $datum = $jsonHashRef->{'Results'}->[0]->{'TestResults'}->{$testTypes[0]}->{$marker}->{'MarkerCall'} // undef;

		if(!defined $datum){
			#however, the metadata, such as "ST" for an MLST scheme as follows:
			$datum = $jsonHashRef->{'Results'}->[0]->{'Metadata'}->{$testTypes[0]}->[0]->{$marker};
		}

		#not allowed to have a database column start with a number
		#therefore add the assay name as a prefix to all 
		#e.g. 'crispol_', 'amr_'
		#remove all non-word characters
		#the schema is set up the same way
		#the names must match, otherwise there will be an error
		
		my $marker = $assay . '_'. lc($marker);
		$marker =~ s/\W//g;
		$self->logger->debug("marker:$marker");		
		
		#if no marker name, skip
		#if no data, input 0 as null
		
		if(!defined $marker || $marker eq ''){
			$self->logger->debug("SKIPPING: $marker");
			next;
		}

		if(!defined $datum || $datum eq '' || $datum =~ m/^\R/){
			$datum=0;
		}
						
		#remove end-of line characters
		$marker =~ s/\R//g;
		$datum =~ s/\R//g;
		
		$self->logger->debug("$marker:$datum");
		#add to database
		$assayRow->$marker($datum);
	}
	$assayRow->update_or_insert();
}


sub _getAllMarkersForTest{
	my $self=shift;
	my $jsonHashRef=shift;
	my $testName = shift;

	#Metadata and markers data are in different JSON levels
	#combine them as we need to report it all

	my @markers =keys %{$jsonHashRef->{'Results'}->[0]->{'TestResults'}->{$testName}};
	my @metaMarkers = keys %{$jsonHashRef->{'Results'}->[0]->{'Metadata'}->{$testName}->[0]};

	return [@markers,@metaMarkers];
}


1;