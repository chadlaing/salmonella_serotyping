#!/usr/bin/env perl

package Modules::PhylogenyManipulation;

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../";
use IO::File;
use File::Basename;
use File::Slurp;
use File::Path qw(make_path remove_tree);
use Log::Log4perl qw(:easy);
#use parent 'Modules::STyper_Super';
use Role::Tiny::With;

with 'Roles::Database';
with 'Roles::Settings';
Log::Log4perl->easy_init($DEBUG);

#object creation
sub new {
	my ($class) = shift;
	my $self = {};
	bless( $self, $class );
	$self->_initialize(@_);
	return $self;
}

sub logger{
	my $self=shift;
	$self->{'_logger'}=shift // return $self->{'_logger'};
}

sub panseqBatchFile{
	my $self=shift;
	$self->{'_panseqBatchFile'}=shift // return $self->{'_panseqBatchFile'};
}

sub strainDirectory{
	my $self=shift;
	$self->{'_strainDirectory'}=shift // return $self->{'_strainDirectory'};
}

sub phylipAlignmentFile{
	my $self=shift;
	$self->{'_phylipAlignmentFile'}=shift // return $self->{'_phylipAlignmentFile'};
}

sub coreAlignmentInfo{
	my $self=shift;
	$self->{'_coreAlignmentInfo'}=shift // return $self->{'_coreAlignmentInfo'};
}

sub panseqOutputDirectory{
	my $self=shift;
	$self->{'_panseqOutputDirectory'}=shift // return $self->{'_panseqOutputDirectory'};
}

sub _initialize{
	my $self=shift;

	$self->logger(Log::Log4perl->get_logger());
	$self->logger->info("Logger initialized in Modules::PhylogenyManipulation");

	my %params = @_;

	#connect db via the Roles::Database role unless dbixClass object passed
	if($params{'dbixSchema'}){
		$self->dbixSchema($params{'dbixSchema'});
	}
	else{
		$self->connectDatabase();
	}

	#get script location via File::Basename
	my $SCRIPT_LOCATION = dirname(__FILE__);

	#load config file if it is not loaded
	unless(defined $self->outputDirectory){
		$self->loadConfigFile("$SCRIPT_LOCATION/../all_settings.config");
	}


	#if tree is undefined, set blank values
	my $treeRow = $self->dbixSchema->resultset('Tree')->find({treeid=>1}) // undef;
	if(defined $treeRow and $treeRow->tree ne ''){
		$self->logger->info("treeid 1 found");
		#do nothing, it exists
	}
	else{
		$self->logger->info("Initializing the tree table");
		$self->_initializeTreeTable();
		$self->addTree($self->newickCore);
	}

	#temp setting of output directory
	# $self->outputDirectory('/home/chad/workspace/salmonella_serotyping/App/Output/');
	# $self->panseqOutputDirectory('/home/chad/panseq/output/salmonella_serotyping_as_on_webserver/');
	# $self->panseqBatchFile('/home/chad/workspace/panseq_dev/Panseq2/Batch/core_config_salmonella_serotyping.txt');
	# $self->phylipAlignmentFile($self->panseqOutputDirectory . 'core_alignment.phylip');
	# $self->coreAlignmentInfo($self->panseqOutputDirectory . 'core_alignment_info.txt');
	# $self->strainDirectory('/home/chad/allDatabaseStrains/');
}

sub _initializeTreeTable{
	my $self=shift;

	my $initRow = $self->dbixSchema->resultset('Tree')->create(
			{
				'tree'=>'',
				'pangenome'=>'',
				'alignment'=>''
			}
		);
}

sub addTree{
	#adds the newick file to the database
	my $self=shift;
	my $treeData=shift // die "Filename required in addTree\n";

	#allow either a file name or the tree in newick format
	my $fileData;
	if($treeData =~ m/^\(/){
		#means it is a newick file
		#raw newick tree
		$fileData = $treeData;
	}else{
		#with File::Slurp
		$fileData = read_file($treeData);
	}
	
	#the tree table has only one row, containing the newick tree and the pangenome
	my $treeRow = $self->dbixSchema->resultset('Tree')->find({treeid=>1}) // die "Cannot find treeid 1 $!";
	$treeRow->tree($fileData);
	$treeRow->update_or_insert();
}

sub addPanGenome{
	my $self=shift;
	my $fileName=shift // die "Filename required in addTree\n";

	#with File::Slurp
	my $fileData = read_file($fileName);

	#the tree table has only one row, containing the newick tree and the pangenome
	my $treeRow = $self->dbixSchema->resultset('Tree')->find({treeid=>1}) // die "Cannot find treeid 1 $!";
	$treeRow->pangenome($fileData);
	$treeRow->update_or_insert();
}

sub addAlignment{
	my $self=shift;
	my $fileName=shift // die "Filename required in addTree\n";

	#with File::Slurp
	my $fileData = read_file($fileName);

	#the tree table has only one row, containing the newick tree and the pangenome
	my $treeRow = $self->dbixSchema->resultset('Tree')->find({treeid=>1}) // die "Cannot find treeid 1 $!";
	$treeRow->alignment($fileData);
	$treeRow->update_or_insert();
}
sub findNovelRegions{
	my $self=shift;
}

sub createNewAlignment{
	my $self = shift;

	#collect all files from database into an output directory for panseq
	#include the lcl|strainid| in each
	#$self->strainDirectory is populated by the following
	$self->populateDirectory();
	
	#after population, run Panseq, using the pan-genome file specified
	$self->runPanseq();	
}

sub runPanseq{
	my $self=shift;

	#get script location via File::Basename
	my $SCRIPT_LOCATION = dirname(__FILE__);
	my $systemLine = "perl $SCRIPT_LOCATION/../../panseq_dev/Panseq2/lib/core_accessory.pl " . $self->panseqBatchFile;
	system($systemLine);
}

sub createNewTree{
	my $self=shift;

	#the new phylip core_alignment.phylip is in the output directory
	#we need to read it in, convert it to fasta format, and run FastTree on it
	#convert the newick tree from FastTree (with Panseq names)
	#to the strainids from the database
	#this information is in the core_alignment_info.txt file
	#stored as $self->coreAlignmentInfo
	#then store the new newick tree in the database

	#create fasta alignment file
	my $fastaAlignmentFile = $self->outputDirectory . 'fastaAlignmentForFastTree.fasta';
	my $fastaFH = IO::File->new('>'. $fastaAlignmentFile) or die "$!";
	$fastaFH->print($self->_getFastaFromPhylip);
	$fastaFH->close();

	my $originalTree = $self->runFastTree($fastaAlignmentFile);

	#convert the names given by Panseq to the strainid from the database
	my $finalTree = $self->_convertPanseqTreeToDatabaseTree($originalTree);

	#finally, add the correct newick file to the database
	$self->addTree($finalTree);
}

sub _convertPanseqTreeToDatabaseTree{
	my $self=shift;
	my $treeFile = shift;

	unless(defined $self->coreAlignmentInfo && defined $treeFile){
		print "Missing parameters. Correct usage is:\n" ,
			'perl treeNumberToName.pl <tree_file_name> <info_file_name> > <output_file_name>' . "\n";
		exit(1);
	}

	
	my $infoFH = IO::File->new('<' . $self->coreAlignmentInfo) or die "$!";

	#with File::Slurp
	my $treeAsLine = read_file($treeFile);

	my $counter=0;
	while(my $line = $infoFH->getline){
		if($counter==0){
			$counter++;
			next;
		}
		
		$line =~s/\R//g;
		my @la= split('\t',$line);
		my $number = $la[0];
		my $name = $la[1];	
		
		#error proof name
		$name =~ s/[\s\:]/_/g;

		#substitute
		if($treeAsLine =~ /[\)\(\,\s]\Q$number\E[\d\-\.]+[\,\)\(]/){
			$treeAsLine =~ s/([\)\(\,\s])\Q$number\E([\d\-\.]+[\,\)\(])/$1$name\:$2/;
		}
	}
	$infoFH->close();
	return $treeAsLine;
}

sub _getFastaFromPhylip{
	my $self= shift;	

	my $inFH = IO::File->new('<'. $self->phylipAlignmentFile) or die "$!";

	my $counter=0;
	my $fastaAlignment='';
	while(my $line=$inFH->getline){
		if($counter==0){
			$counter++;
			next;
			#skip the phylip header
		}

		if($line =~ m/^(\d+)/){
			my $id = $1;
			$line =~ s/^\d+\s+/>$id\n/;
		}
		$fastaAlignment .=$line;
	}
	$inFH->close();
	return $fastaAlignment;
}

sub runFastTree{
	my $self=shift;
	my $fastaAlignmentFile = shift;

	my $newickOutputFile = $self->outputDirectory . 'fastTree.tre';
	$self->logger->info("newickOutputFile: $newickOutputFile");
	my $systemLine = 'FastTreeMP -nt -fastest -nosupport < ' . $fastaAlignmentFile . ' > ' . $newickOutputFile;
	#system($systemLine);
	return $newickOutputFile;
}

sub treeAsFile{
	my $self=shift;

	#creates a new instance each time it is called
	#return the new instance
	
	my $fileName = $self->outputDirectory . 'tree_from_database.new';
	my $tree = $self->dbixSchema->resultset('Tree')->find({treeid=>1})->tree;

	my $outFH = IO::File->new('>' . $fileName) or die "$!";
	$outFH->print($tree);
	$outFH->close();
	return $fileName;
}

sub populateDirectory{
	my $self=shift;
	#with File::Path
	if(-d $self->strainDirectory){
		remove_tree($self->strainDirectory) or die "Could not remove tree\n";
	}
	make_path($self->strainDirectory) or die "Cannot create directory " . $self->strainDirectory;

	my $strainTable = $self->dbixSchema->resultset('Strain');

	while(my $strain = $strainTable->next){
		my $outFH = IO::File->new('>' . $self->strainDirectory . $strain->strainid . '_lcl.fasta') or die "Cannot create strain file $!";
		$outFH->print($self->_getLclFile($strain));
		$outFH->close();
	}
}

sub _getLclFile{
	my $self=shift;
	my $strain = shift;

	my $sequence = $strain->sequence;
	my $strainid = $strain->strainid;
	$self->logger->info("Strain id: $strainid first chars" . substr($sequence, 0,10));
	$sequence =~ s/>/>lcl\|$strainid\|/g;
	return $sequence;
}

sub createTempTree{
	my $self=shift;
	my $arrayRefOfStrains=shift;
	#add the new strain as the outgroup until a new phylogeny can be generated

	#get the tree in newick format
	my $tree = $self->dbixSchema->resultset('Tree')->find({treeid=>1})->tree;

	$tree =~s/\;//;

	#add the new
	foreach my $newStrain(@{$arrayRefOfStrains}){
		$tree = '(' . $newStrain . ',' . $tree . ')';
	}

	$tree .= ';';

	$self->addTree($tree);
}