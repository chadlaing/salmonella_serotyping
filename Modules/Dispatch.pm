#!/usr/bin/env perl
package Modules::Dispatch;

# mod_rewrite alters the PATH_INFO by turning it into a file system path,
# so we repair it.
#from https://metacpan.org/module/CGI::Application::Dispatch#DISPATCH-TABLE

$ENV{PATH_INFO} =~ s/^$ENV{DOCUMENT_ROOT}// if defined $ENV{PATH_INFO};

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/..";
use parent qw/CGI::Application::Dispatch/;
use File::Basename;

#get script location via File::Basename
my $SCRIPT_LOCATION = dirname(__FILE__);


sub dispatch_args {
    return {
        prefix  => 'Modules',
        args_to_new=>{
        	TMPL_PATH=>"$SCRIPT_LOCATION/../App/Templates/"
        },
        table   => [
        	''	=>	{app=>'Home',rm=>'display'},
        	'/'	=>	{app=>'Home',rm=>'display'},
        	'/home/d' => {app=>'Home',rm=>'display'},
            '/home/d/' => {app=>'Home',rm=>'display'},
            '/home/' => {app=>'Home',rm=>'display'},
            '/home' => {app=>'Home',rm=>'display'},
        	'/assays/d' => {app=>'Assays',rm=>'display'},
        	'/assays/r' => {app=>'Assays',rm=>'retrieve'},
            '/upload/d' => {app=>'Upload',rm=>'display'},
            '/upload/u' => {app=>'Upload',rm=>'upload'},
            '/upload/f' => {app=>'Upload',rm=>'finished'}
        ],
    };
}

1;

