#!/usr/bin/env perl
package Modules::Assays;

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/..";
use STyper::Schema;
use Modules::TreeGenerator;
use File::Basename;
use Role::Tiny::With;
use Modules::PhylogenyManipulation;
use Data::Dumper;
use parent 'Modules::STyper_Super';
use feature 'switch';

#add database role
with 'Roles::Database';


sub setup{
	my $self=shift;

	$self->start_mode('d');
	$self->run_modes(
		'display'=>'display',
		'retrieve'=>'retrieve'
	);

	$self->assayNames({
		'mlst'=>'MLST',
		'mlva'=>'MLVA',
		'amr'=>'AMR',
		'crispol'=>'CRISPOL',
		'serogroup'=>'Serogroup',
		'serovar'=>'Serovar',
		'h0'=>'H0',
		'h1'=>'H1',
		'h2'=>'H2',
		'antigen'=>'Antigen',
		'misc'=>'Misc'
	});

	#connect dbixSchema
	#uses the Roles::Settings info in Modules::STyper_Super
	unless(defined $self->dbixSchema){
		$self->connectDatabase();
	}

	#init hash
	$self->_typeHash({});

	$self->logger->info("In assays!");
}

sub assayNames{
	my $self=shift;
	$self->{'_assayNames'}=shift // return $self->{'_assayNames'};
}

sub _typeHash{
	my $self=shift;
	$self->{'__typeHash'}=shift // return $self->{'__typeHash'};
}


#Display the home page
sub display{
	my $self=shift;
	
	
	#note that Strain refers to the Strain.pm module, not the 'strain' table in the database
	#'strain' is defined as the table in Strain.pm, and all table/column names appear to require lowercase
	#my @strains = $schema->resultset('Strain')->all();
	#use 'columns' to avoid loading all of the data such as the genome sequence
	#this allows an acceptable speed for displaying the data
	my $allStrains = $self->dbixSchema->resultset('Strain')->search(
			undef,
			{
				columns=>[qw/strainid name/],
			}
		);
	my $loop_ALL_STRAINS_REF = $self->_getAllStrainNames($allStrains);

	my $template = $self->load_tmpl('assays.tmpl', die_on_bad_params=>0);
	$template->param(ALL_STRAINS=>$loop_ALL_STRAINS_REF);
	return $template->output();
}

sub _getAllStrainNames{
	my $self=shift;
	my $allStrains = shift;

	my %sortedRowDataHash;
	my @finalData;
	while(my $strainRow = $allStrains->next){
		my %rowData;

		#we want to filter the names to something more readable
		#Salmonella enterica subsp. enterica serovar Enteritidis str. 629164-26 SEEE6426_20, whole genome shotgun sequence
		#is too long
		#we will:
		#	-get rid of any leading 'Salmonella enterica subsp. enterica serovar' and everything following a comma
		#   -and everything following (C|c)ontig
		#	-the plasmid in the name only indicates that we have included the plasmid in the same file as the genomes, 
		#		not that it is a plasmid sequence

		my $tempName = $self->_getDisplayName($strainRow->name);
		
		$rowData{'STRAIN_ID'}=$strainRow->strainid;
		$rowData{'STRAIN_NAME'}=$tempName;
		
		#store each row in a hash, with name as key
		#after all data loaded, sort based on key and add to @loop_ALL_STRAINS
		$sortedRowDataHash{$rowData{'STRAIN_NAME'}}=\%rowData;
		
	}
	foreach my $strainRow(sort keys %sortedRowDataHash){
		#add each row as an array ref; TMPL_LOOP will use each arrayref for one iteration
		push(@finalData, $sortedRowDataHash{$strainRow});
	}
	return \@finalData;
}

=head2 _getColumnNamesFromRow

Takes a resultset row and gets
only the column names, creates an array and send back the arrayRef.
Can use $self->_getColumnNamesFromRow($resultSet->single()) for the most efficient generation.

=cut

sub _getColumnNamesFromRow{
	my $self=shift;
	my $row=shift;

	my @columnNames;
	my %columnHash = $row->get_columns();
	foreach my $column(sort keys %columnHash){
		push @columnNames, $column;
	}
	return \@columnNames;
}



sub _getData{
	my $self=shift;
	my $selectedStrainsInTreeOrder=shift;
	my $assay=shift;

	my $assayTable = $self->dbixSchema->resultset($assay);

	# my ($h1,$h2);
	# if($assay eq 'Serovar'){
	# 	$h1 = $self->dbixSchema->resultset($assay);
	# 	$h2 = $self->dbixSchema->resultset($assay);
	# }

	#get a single row from the resultset for column names
	my $columnNames = $self->_getColumnNamesFromRow($assayTable->find({strainid=>$selectedStrainsInTreeOrder->[0]}));

	my $columnNamesRow= $self->_getColumnNamesRow($columnNames, $assay);

	my @dataForAssay=({ROW=>$columnNamesRow});

	foreach my $selectedStrain(@{$selectedStrainsInTreeOrder}){
		my $row = $assayTable->find({strainid=>$selectedStrain});
		my @strainData=({ROW_DATUM=>$self->_getSummaryType($assay,$row)});

		foreach my $column(@{$columnNames}){
			if($column =~ m/verbose_output|strainid/){
					next;
			}	
			push @strainData,{ROW_DATUM=>$row->get_column($column)};			
		}
		push @dataForAssay, {ROW=>\@strainData};

	}
	return \@dataForAssay;

}

=head2 _getSummaryTypeName

Returns the "sequence type" name for a given assay.
Eg. MLST returns ST for sequence type.

=cut

sub _getSummaryTypeName{
	my $self =shift;
	my $assay = shift;

	my %summaryName=(
		MLST=>'ST',
		MLVA=>'Type',
		Serovar=>'Serovar',
		CRISPOL=>'Type'
	);

	if($summaryName{$assay}){
		return $summaryName{$assay};
	}
	else{
		return 'ST';
	}
}



=head2 _getColumnNamesRow

Creates the column headers for the display of the results.
We need the "Sequence Type" entry to be first.

=cut

sub _getColumnNamesRow{
	my $self=shift;
	my $columnNames = shift;
	my $assay = shift;

	my @columnNamesRow =(
		{ROW_DATUM=>$self->_getSummaryTypeName($assay)}
	);

	foreach my $column(@{$columnNames}){
		if($column =~ m/strainid|output/){
			next;
		}

		my $columnTrim = $column;
		$columnTrim =~ s/^[a-z0-1]+_//;
		push @columnNamesRow, {ROW_DATUM=>$columnTrim};
	}
	return \@columnNamesRow;
}

=head2 _getLeafOrderAllIds

Appends any strain IDs not yet in the tree to the end of the array.
This allows the typing results to still be displayed.

=cut

sub _getLeafOrderAllIds{
	my $self = shift;
	my $all = shift;
	my $tree = shift;

	my %treeIds=();

	foreach my $treeId(@{$tree}){
		$treeIds{$treeId}=1;
		$self->logger->debug("treeid: $treeId");
	}

	foreach my $allId(@{$all}){
		unless(defined $treeIds{$allId}){
			$self->logger->debug("adding $allId to the tree");
			push @{$tree},$allId;
		}
	}
	return $tree;
}

sub retrieve{
	my $self=shift;

	#get the form data
	my $q = $self->query();

	#select only the strainid that were chosen by the user via the form
	#the form returns the primary keys (which are integers)
	#every database table with strains has the same strainid
	my $strainTable = $self->dbixSchema->resultset('Strain');

	#get an array of all needed strain ids to use in grabbing the data from the database
	#this prevents constant database access if we were to iterate over each strain id and then each assay table
	my $selectedStrainsRef = $self->_getSelectedStrains($q);
	$self->logger->info(@{$selectedStrainsRef});

	#get whole-genome phylogeny
	my $treeMaker= $self->_createTree($selectedStrainsRef);

	my $selectedStrainsInTreeOrderForTemplate = $self->_getSelectedStrainsInTreeOrderForTemplate($strainTable,$treeMaker);

	my $leafLabelOrder = $treeMaker->leafLabelOrder;
	#if there are any IDs not in the tree (because the tree has not yet updated)
	#we need to append those IDs to the end of the list, to still get their typing data.
	#if the database has no entry for an entire test, retrieval fails
	#NEED TO ACCOUNT FOR THIS IN MIST.pm. ensure each test has an entry for each strain
	my $leafOrderAllIds = $self->_getLeafOrderAllIds($selectedStrainsRef,$leafLabelOrder);

	my $template = $self->load_tmpl('result_table_div.tmpl', die_on_bad_params=>0,loop_context_vars=>0,global_vars=>0);
	$template->param(TREE_FILE=>'/styper/Output/tree.svg');
	$template->param(SELECTED_STRAINS=>$selectedStrainsInTreeOrderForTemplate);
	$template->param(MLST_DATA=>$self->_getData($leafOrderAllIds,'MLST'));
	$template->param(SEROVAR_DATA=>$self->_getData($leafOrderAllIds,'Serovar'));
	$template->param(CRISPOL_DATA=>$self->_getData($leafOrderAllIds,'CRISPOL'));
	$template->param(MLVA_DATA=>$self->_getData($leafOrderAllIds,'MLVA'));
	# $template->param(AMR_DATA=>$self->_getData($leafLabelOrder,'AMR'));
	$template->param(TABS_HEIGHT=>$self->_getTabsHeight($leafOrderAllIds));
	return $template->output();

}

=head2 _getTabsHeight

The CSS tabs class requires a minimum height to be set.
This has to be calculated dynamically, which is done here.

=cut

sub _getTabsHeight{
	my $self=shift;

	my $strainNames = shift;
	my $numberOfStrains = @{$strainNames};
	my $height = 7 + ($numberOfStrains * 4);
	return $height;
}

sub _createTree{
	my $self=shift;
	my $selectedStrainsRef=shift;
	
	#get a fresh tree from the database
	my $newickFile = Modules::PhylogenyManipulation->new(
		'dbixSchema'=>$self->dbixSchema
	);

	#generate the tree.svg given an array of strainids
	my $treeMaker = Modules::TreeGenerator->new(
		'labels'=>$selectedStrainsRef,
		'newickFile'=>$newickFile->treeAsFile,
		'outputDirectory'=>$self->outputDirectory
	);
	$treeMaker->createTree();
	return $treeMaker;
}

=head2 _getSummaryType

Takes all database columns for the current assay and the given strain and passes them as $strainHashRef.
We want to return the "summary" type from these data eg. the ST for MLST, or create our own for other schemes.

=cut

sub _getSummaryType{
	my $self=shift;
	my $assay =shift;
	my $row=shift;

	my $summaryValue;

	if($assay =~ m/(CRISPOL|MLVA)/){
		$summaryValue = $self->_createType($assay,$row);
	}
	elsif($assay =~ m/Serovar/){
		$summaryValue = $self->_getSerotype($row);
	}
	elsif($assay =~ m/MLST/){		
		$summaryValue =$row->mlst_st;
	}

	return $summaryValue;
}


sub _getSerotype{
	my $self=shift;
	my $row =shift;

	return $row->serovar_serovar;
}


=head2 _createType

Takes all loci for a scheme, sorts in alphabetical order, and returns a type number
based on concatenated values of all loci.
We do not include verbose_output or strainid.

=cut

sub _createType{
	my $self=shift;
	my $assay = shift;
	my $row = shift;

	#we sort all of the keys from the row to ensure the same order everytime
	#next we get the value of the hash for each key
	#the values from each key are joined with no delimiter to create the $typeString
	my $typeString='';
	foreach my $locus (sort keys %{$row}){
		if($locus =~ /(verbose_output|strainid)/){
			next;
		}
		else{
			$typeString .= $row->{$locus} // '';
		}
	}
	
	if(defined $self->_typeHash->{$assay}->{$typeString}){
		#good, return
	}
	else{
		my $numberOfTypes = (scalar keys %{$self->_typeHash->{$assay}}) // 0;
		$numberOfTypes++;
		$self->_typeHash->{$assay}->{$typeString}=($numberOfTypes);
	}
	return $self->_typeHash->{$assay}->{$typeString};
}



sub _getSelectedStrains{
	my $self=shift;
	my $q=shift;

	my @selectedStrains;
	foreach my $strainid($q->param('allStrains')){
		$self->logger->debug("strainid: $strainid");
		push @selectedStrains, $strainid;
	}	
	return \@selectedStrains;
}

sub _getSelectedStrainsInTreeOrderForTemplate{
	my $self = shift;
	my $strainTable=shift;
	my $treeMaker=shift;

	my $gb = $strainTable->search(
		{},
		{
			columns=>[qw /strainid name/]
		}
	);

	my @selectedStrainsForTable = map{
		{
				'SELECTED_STRAIN'=>$self->_getDisplayName($gb->find({strainid=>$_})->name)
		}
	} @{$treeMaker->leafLabelOrder};
	return \@selectedStrainsForTable;
}

sub _getDisplayName{
	my $self = shift;
	my $tempName = shift;

	$tempName =~ s/Salmonella enterica subsp. enterica serovar //;
	$tempName =~ s/,.+//;
	$tempName =~ s/.(C|c)ontig.+//;
	$tempName =~ s/.(C|c)omplete.+//;
	$tempName =~ s/.(P|p)lasmid.*//;
	return $tempName;
}

1;






