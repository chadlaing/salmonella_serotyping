#!/usr/bin/env perl

package Modules::AddToDatabase;

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../";
use IO::File;
use File::Basename;
use Role::Tiny::With;
use Modules::SequenceName;
use Log::Log4perl;

with 'Roles::Database';
with 'Roles::Settings';

#object creation
sub new {
	my ($class) = shift;
	my $self = {};
	bless( $self, $class );
	$self->_initialize(@_);
	return $self;
}


sub _initialize{
	my $self=shift;

	$self->logger(Log::Log4perl->get_logger()); 
	$self->logger->info("Logger initialized in AddToDatabase");

	my %params = @_;

	if($params{'configFile'}){
		$self->loadConfigFile($params{'configFile'});
	}
	
	#connect db via the Roles::Database role unless dbixSchema object passed
	if($params{'dbixSchema'}){
		$self->dbixSchema($params{'dbixSchema'});
	}
	else{
		$self->connectDatabase();
	}

	#init array of added strainid
	$self->addedStrains([]);

	$self->_gatherStrains($params{'input'});
}

sub logger{
	my $self=shift;
	$self->{'_logger'}=shift // return $self->{'_logger'};
}


sub duplicateStrain{
	my $self=shift;
	$self->{'_duplicateStrains'}=shift // return $self->{'_duplicateStrains'};
}

sub _missing{
	my $self=shift;
	my $param=shift;

	$self->logger->fatal("Missing $param in AddToDatabase");
	die "$!";
}

sub inputStrains{
	my $self=shift;
	$self->{'_inputStrains'}=shift // return $self->{'_inputStrains'};
}

sub inputDirectory{
	my $self=shift;
	$self->{'_inputDirectory'}=shift // return $self->{'_inputDirectory'};
}

sub addedStrains{
	my $self=shift;
	$self->{'_addedStrains'}=shift // return $self->{'_addedStrains'};
}

sub _gatherStrains{
	my $self=shift;
	my $input = shift;

	if(-f $input){
		#get location via File::Basename
		my $directory = dirname($input) . '/';
		my $fileName = fileparse($input);

		$self->inputDirectory($directory);
		$self->inputStrains([$fileName]);
	}
	elsif(-d $input){
		#look through given directory, collect all files

		opendir DIR, $input;
		my @strains = grep { $_ ne '.' && $_ ne '..' && $_ ne '.directory' } readdir DIR;
		closedir DIR;

		$self->inputDirectory($input);
		$self->inputStrains(\@strains);
	}
	else{
		die "Neither a file nor directory\n";
	}
}

sub addToDatabase{
	my $self=shift;

	#search for name, filename and gb to avoid duplicates
	#restrict to these to avoid pulling down sequence data
	#search this small set for particular names etc.
	my $searchSet = $self->dbixSchema->resultset('Strain')->search(
		{},
		{
			columns =>[qw /filename gb name strainid/]
		}

	);

	foreach my $strain(@{$self->inputStrains}){
		#get gb, name and sequence for file
		#add to database if new
		my $filename = $self->inputDirectory . $strain;

		my $inFH = IO::File->new('<'. $filename) or (die "Cannot open $filename\n");
		my @sequence = $inFH->getlines();
		$inFH->close();
		
		my $sn = Modules::SequenceName->new($sequence[0]);
		my $gb = $sn->name();

		my $name;
		if($sequence[0] =~ m/^>.+\|(.+)/){
			$name=$1;
		}
		else{
			$name = $sequence[0];
		}
		
		my $duplicateValue = $searchSet->search({
				filename=>$filename,
				name=>$name,
				gb=>$gb
			}
		);
			
		if(defined $duplicateValue->next){
			#if a strain has previously been added to the database,
			#keep a list of the duplicate values so that they are not used
			#in downstream applications such as adding to the temp newick file
			$self->logger->info("Found gb: ". $gb . " name: " . $name  . ", Skipping. $sequence[0]");
			$self->duplicateStrain(1), 
			next;
		}

		my $newStrain = $self->dbixSchema->resultset('Strain')->create(
			{
				filename=>$filename,
				name=>$name,
				sequence=>join('',@sequence),
				gb=>$gb
			}
		);		
		push @{$self->addedStrains},$newStrain->strainid;

		$self->logger->debug("Newly added strain; Name:" . $name . " strainid: " . $newStrain->strainid);

	} #end of foreach
} 


1;


