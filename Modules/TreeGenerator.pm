#!/usr/bin/env perl
package Modules::TreeGenerator;

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/..";
use IO::File;
use Log::Log4perl qw(:easy);

#object creation
sub new {
	my ($class) = shift;
	my $self = {};
	bless( $self, $class );
	$self->_initialize(@_);
	return $self;
}

sub logger{
	my $self=shift;
	$self->{'_logger'}=shift // return $self->{'_logger'};
}

sub strainLabels{
	my $self=shift;
	$self->{'_strainLabels'}=shift // return $self->{'_strainLabels'};
}

sub leafLabelOrder{
	my $self=shift;
	$self->{'_leafLabelOrder'}=shift // return $self->{'_leafLabelOrder'};
}

sub newickFile{
	my $self=shift;
	$self->{'_newickFile'}=shift // return $self->{'_newickFile'};
}

sub outputDirectory{
	my $self=shift;
	$self->{'_outputDirectory'}=shift // return $self->{'_outputDirectory'};
}

sub outputTree{
	my $self=shift;
	$self->{'_outputTree'}=shift // return $self->{'_outputTree'};
}


sub _initialize{
	my $self=shift;

	$self->logger(Log::Log4perl->get_logger());
	$self->logger->info("Logger initialized in Modules::PhylogenyManipulation");

	my %params = @_;

	$self->strainLabels($self->_createLabelsHash($params{'labels'})) // $self->_missingRequired("labels");
	$self->newickFile($params{'newickFile'}) // $self->_missingRequired("Newick file");
	$self->outputDirectory($params{'outputDirectory'}) // $self->outputDirectory('');
}

sub _createLabelsHash{
	my $self=shift;
	my $labels=shift;

	my %labelHash = map {$_=>1} @{$labels};
	return \%labelHash;
}

sub createTree{
	my $self=shift;

	my $fileName = $self->outputDirectory . 'treegenerator.labels';
	my $systemLine = 'nw_labels ' . $self->newickFile . ' > ' . $fileName;

	#run the newick utilities to extract all node labels
	#note this includes the support values (0-1)
	#we want to exclude all values not in $self->strainLabels greater than 1
	system($systemLine);

	my $tempFH = IO::File->new('<' . $fileName) or die "$!";

	my @namesToPrune;
	my @orderedLabels;
	while(my $line = $tempFH->getline){
		#remove end of line characters
		$line =~ s/\R//g;

		if($line < 1){
		 	next;
		}

		if($self->strainLabels->{$line}){
			next;
		}
		else{
			#remove the label that was not selected by the user
			push @namesToPrune,$line;
		}
	}
	$tempFH->close();
	#unlink $fileName;

	$self->_pruneTree(\@namesToPrune);
	$self->getLeafLabelOrder();
}

sub _pruneTree{
	my $self=shift;
	my $namesArrayRef=shift;

	$self->outputTree($self->outputDirectory . 'tree.svg');

	my $systemLine;
	if($namesArrayRef->[0]){
		#remove the names
		$systemLine = 'nw_prune ' . $self->newickFile . ' ';

		foreach my $name(@{$namesArrayRef}){
			$systemLine .=$name . ' ';
		}

		$systemLine .= '| nw_topology - ';
	}
	else{
		$systemLine= 'nw_topology ' . $self->newickFile . ' ';
	}

	$systemLine .= '| nw_order -c a - | nw_display -s -S -W 0 -w 100 -v 40 -i "font-size:0" -b "font-size:0" -l "font-size:0" - > ' . $self->outputTree;

	system($systemLine);
}

sub getLeafLabelOrder{
	my $self=shift;

	#we get the whole tree as a string so that it can be processed by the regex in the while loop
	#we use the character class [\w=\s\'\.] because a '.' would match all the way to the end of the SVG file
	#and give us only the first leaf-label.
	#finally, the method leafLabelOrder is populated

	my @leafLabelOrder;
	my $treeFH=IO::File->new('<' . $self->outputTree) or die "$!";
	my $tree =join('',$treeFH->getlines());
	$treeFH->close();

	while($tree =~ m/\<text class='leaf-label'[\w=\s\'\.]+\>(\d+)\<\/text\>/gc){
		push @leafLabelOrder,$1;
	}
	$self->leafLabelOrder(\@leafLabelOrder);
}


sub _missingRequired{
	my $self=shift;
	my $param = shift;

	die "TreeGenerator requires $param\n";

}













1;